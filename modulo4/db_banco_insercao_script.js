//db.banco.drop()

use local

db.createCollection("banco")

enderecoClientes =  {
    rua: "Jos? do patr?c?nio",
    numero: "913",
    complemento: "apto 613",
    bairro: "CB",
    cidade: "POA",
    estado: "RS",
    CEP: "00000-000",
    pais: "Brasil"
}

enderecoGerentes =  {
    rua: "Jos? do patr?c?nio",
    numero: "913",
    complemento: "apto 613",
    bairro: "CB",
    cidade: "POA",
    estado: "RS",
    CEP: "00000-000",
    pais: "Brasil"
}

cliente1 = {
    nome: "cliente1",
    cpf: "000.000.000-01",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}    

cliente2 = {
    nome: "cliente2",
    cpf: "000.000.000-02",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}    

cliente3 = {
    nome: "cliente3",
    cpf: "000.000.000-03",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}  

cliente4 = {
    nome: "cliente4",
    cpf: "000.000.000-04",
    estadoCivil: "",
    dataDeNnascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente5 = {
    nome: "cliente5",
    cpf: "000.000.000-05",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente6 = {
    nome: "cliente6",
    cpf: "000.000.000-06",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente7 = {
    nome: "cliente7",
    cpf: "000.000.000-07",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente8 = {
    nome: "cliente8",
    cpf: "000.000.000-08",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente9 = {
    nome: "cliente9",
    cpf: "000.000.000-09",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente10 = {
    nome: "cliente10",
    cpf: "000.000.000-10",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

gerente1 = {
    nome: "gerente01",
    cpf: "000.000.001-01",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    codigoFuncionario: "0001",
    tipoDeGerente: "GC",
    endereco: enderecoClientes
}

conta1 = {
    codigo:"0001",
    saldo: 0,
    tipo_conta: "PJ",
    movimentacoes:[],
    cliente:[ cliente1, cliente2, cliente3 ],
    gerente:[ gerente1 ] 
}    

conta2 = {
    codigo:"0002",
    saldo: 0,
    tipo_conta: "CONJ",
    movimentacoes:[],
    cliente:[ cliente10, cliente9 ],
    gerente:[ gerente1 ] 
} 

conta3 = {
    codigo:"0003",
    saldo: 0,
    tipo_conta: "PJ",
    movimentacoes:[],
    cliente:[ cliente4, cliente5, cliente6 ],
    gerente:[ gerente1 ] 
} 

conta4 = {
    codigo:"0004",
    saldo: 0,
    tipo_conta: "CONJ",
    movimentacoes:[],
    cliente:[ cliente7, cliente8],
    gerente:[ gerente1 ] 
} 

conta5 = {
    codigo:"0005",
    saldo: 0,
    tipo_conta: "CONJ",
    movimentacoes:[],
    cliente:[ cliente6, cliente5],
    gerente:[ gerente1 ] 
}

conta6 = {
    codigo:"0006",
    saldo: 0,
    tipo_conta: "PF",
    movimentacoes:[],
    cliente:[ cliente1 ],
    gerente:[ gerente1 ] 
}

conta7 = {
    codigo:"0007",
    saldo: 0,
    tipo_conta: "PF",
    movimentacoes:[],
    cliente:[ cliente2 ],
    gerente:[ gerente1 ] 
}

conta8 = {
    codigo:"0008",
    saldo: 0,
    tipo_conta: "PF",
    movimentacoes:[],
    cliente:[ cliente3 ],
    gerente:[ gerente1 ] 
}

consolidacaoVazia = {
   saldoAtual: 0,
   saques: 0,
   depositos: 0,
   numeroDeCorrentistas: 0
} 

bancoAlfa = 
{
    codigo: "011",
    nome:"Alfa",
    agencia: [
        {
            codigo: "0001",
            nome: "Web",
            endereco : {
                    rua: "Testando",
                    numero: "55",
                    complemento: "loja 1",
                    bairro: "NA",
                    cidade: "NA",
                    estado: "NA",
                    pais: "Brasil"
           },
           consolidacao: [consolidacaoVazia],
           contas:[
                conta1, conta2
           ]
        },
        {
           codigo: "0002",
           nome: "California",
           endereco : {
                   rua: "Testing",
                   numero: "122",
                   complemento: "",
                   bairro: "Between Hyde and owell Streets",
                   cidade: "San Franscisco",
                   estado: "California",
                   pais: "EUA"
           },
           consolidacao: [consolidacaoVazia],
           contas:[
                conta3, conta4
           ]
        },
        {
            codigo: "0101",
            nome: "Londres",
            endereco : {
                    rua: "Tesing",
                    numero: "525",
                    complemento: "",
                    bairro: "Croydon",
                    cidade: "Londres",
                    estado: "Boroughs",
                    pais: "Englando"
           },
           consolidacao: [consolidacaoVazia],
           contas:[
                conta5, conta6, conta7, conta8
           ]
        } 
    ]    
}

cliente11 = {
    nome: "cliente11",
    cpf: "000.000.000-11",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente12 = {
    nome: "cliente12",
    cpf: "000.000.000-12",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente13 = {
    nome: "cliente13",
    cpf: "000.000.000-13",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente14 = {
    nome: "cliente14",
    cpf: "000.000.000-14",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente15 = {
    nome: "cliente15",
    cpf: "000.000.000-15",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente16 = {
    nome: "cliente16",
    cpf: "000.000.000-16",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente17 = {
    nome: "cliente17",
    cpf: "000.000.000-17",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente18 = {
    nome: "cliente18",
    cpf: "000.000.000-18",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente19 = {
    nome: "cliente19",
    cpf: "000.000.000-19",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente20 = {
    nome: "cliente20",
    cpf: "000.000.000-20",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}


conta9 = {
    codigo:"0009",
    saldo: 0,
    tipo_conta: "PJ",
    movimentacoes:[],
    cliente:[ cliente11, cliente12, cliente13 ],
    gerente:[ gerente1 ] 
}    

conta10 = {
    codigo:"0010",
    saldo: 0,
    tipo_conta: "CONJ",
    movimentacoes:[],
    cliente:[ cliente20, cliente19 ],
    gerente:[ gerente1 ] 
} 

conta11 = {
    codigo:"0011",
    saldo: 0,
    tipo_conta: "PJ",
    movimentacoes:[],
    cliente:[ cliente14, cliente15, cliente16 ],
    gerente:[ gerente1 ] 
} 

conta12 = {
    codigo:"0012",
    saldo: 0,
    tipo_conta: "CONJ",
    movimentacoes:[],
    cliente:[ cliente17, cliente18],
    gerente:[ gerente1 ] 
} 

conta13 = {
    codigo:"0013",
    saldo: 0,
    tipo_conta: "CONJ",
    movimentacoes:[],
    cliente:[ cliente16, cliente15],
    gerente:[ gerente1 ] 
}

conta14 = {
    codigo:"0014",
    saldo: 0,
    tipo_conta: "PF",
    movimentacoes:[],
    cliente:[ cliente11 ],
    gerente:[ gerente1 ] 
}

conta15 = {
    codigo:"0015",
    saldo: 0,
    tipo_conta: "PF",
    movimentacoes:[],
    cliente:[ cliente12 ],
    gerente:[ gerente1 ] 
}

conta16 = {
    codigo:"0016",
    saldo: 0,
    tipo_conta: "PF",
    movimentacoes:[],
    cliente:[ cliente13 ],
    gerente:[ gerente1 ] 
}

bancoBeta = {
    codigo: "241",
    nome:"Beta",
    agencia: [
        {
            codigo: "0001",
            nome: "Web",
            endereco : {
                    rua: "Testando",
                    numero: "55",
                    complemento: "loja 1",
                    bairro: "NA",
                    cidade: "NA",
                    estado: "NA",
                    pais: "Brasil"
           },
           consolidacao: [consolidacaoVazia],
           contas:[
                conta9, conta10, conta11, conta12, conta13, conta14, conta15, conta16
           ]   
        }
   ]    
}

cliente21 = {
    nome: "cliente21",
    cpf: "000.000.000-21",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente22 = {
    nome: "cliente22",
    cpf: "000.000.000-22",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente23 = {
    nome: "cliente23",
    cpf: "000.000.000-23",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente24 = {
    nome: "cliente24",
    cpf: "000.000.000-24",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente25 = {
    nome: "cliente25",
    cpf: "000.000.000-25",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente26 = {
    nome: "cliente26",
    cpf: "000.000.000-26",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente27 = {
    nome: "cliente27",
    cpf: "000.000.000-27",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente28 = {
    nome: "cliente28",
    cpf: "000.000.000-28",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente29 = {
    nome: "cliente29",
    cpf: "000.000.000-29",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

cliente30 = {
    nome: "cliente30",
    cpf: "000.000.000-30",
    estadoCivil: "",
    dataDeNascimento: "01/01/0000",
    endereco: enderecoClientes
}

conta17 = {
    codigo:"0017",
    saldo: 0,
    tipo_conta: "PJ",
    movimentacoes:[],
    cliente:[ cliente21, cliente22, cliente23 ],
    gerente:[ gerente1 ] 
}    

conta18 = {
    codigo:"0018",
    saldo: 0,
    tipo_conta: "CONJ",
    movimentacoes:[],
    cliente:[ cliente30, cliente29 ],
    gerente:[ gerente1 ] 
} 

conta19 = {
    codigo:"0019",
    saldo: 0,
    tipo_conta: "PJ",
    movimentacoes:[],
    cliente:[ cliente24, cliente25, cliente26 ],
    gerente:[ gerente1 ] 
} 

conta20 = {
    codigo:"0020",
    saldo: 0,
    tipo_conta: "CONJ",
    movimentacoes:[],
    cliente:[ cliente27, cliente28],
    gerente:[ gerente1 ] 
} 

conta21 = {
    codigo:"0021",
    saldo: 0,
    tipo_conta: "CONJ",
    movimentacoes:[],
    cliente:[ cliente26, cliente25],
    gerente:[ gerente1 ] 
}

conta22 = {
    codigo:"0022",
    saldo: 0,
    tipo_conta: "PF",
    movimentacoes:[],
    cliente:[ cliente21 ],
    gerente:[ gerente1 ] 
}

conta23 = {
    codigo:"0023",
    saldo: 0,
    tipo_conta: "PF",
    movimentacoes:[],
    cliente:[ cliente22 ],
    gerente:[ gerente1 ] 
}

conta24 = {
    codigo:"0024",
    saldo: 0,
    tipo_conta: "PF",
    movimentacoes:[],
    cliente:[ cliente23 ],
    gerente:[ gerente1 ] 
}


bancoOmega = 
{
    codigo: "307",
    nome:"Omega",
    agencia: [
        {
            codigo: "0001",
            nome: "Web",
            endereco : {
                    rua: "Testando",
                    numero: "55",
                    complemento: "loja 3",
                    bairro: "NA",
                    cidade: "NA",
                    estado: "NA",
                    pais: "Brasil"    
           },
           consolidacao: [consolidacaoVazia],
           contas:[
                conta17, conta18
           ]
        },
        {
            codigo: "8761",
            nome: "Itu",
            endereco : {
                    rua: "Rua do Meio",
                    numero: "2233",
                    complemento: "",
                    bairro: "Qualquer",
                    cidade: "Itu",
                    estado: "S?o Paulo",
                    pais: "Brail"
           },
           consolidacao: [consolidacaoVazia],
           contas:[
                conta19, conta20
           ]
        },
        {
            codigo: "4567",
            nome: "Hermana",
            endereco : {
                    rua: "Rua do boca",
                    numero: "222",
                    complemento: "",
                    bairro: "Caminito",
                    cidade: "Buenos Aires",
                    estado: "Buenos Aires",
                    pais: "Argentina",
           },
           consolidacao: [consolidacaoVazia],
           contas:[
                conta21, conta22, conta23, conta24
           ]
        } 
        
    ]    
}

db.banco.insert( bancoAlfa )
db.banco.insert( bancoBeta )
db.banco.insert( bancoOmega )

//db.banco.find()

//db.banco.find({ nome: { $regex: /a/ } } )

//db.banco.find({ codigo: { $eq: "241" } } ) //equals

//db.banco.find({ codigo: { $ne: "241" } } ) //diferente - not-equals

//db.banco.find({ codigo: { $gt: "241" } } ) //maior que - grater then

//db.banco.find({ codigo: { $lt: "241" } } ) //menor que - lower then

//db.banco.find({ codigo: { $gte: "241" } } ) //maior que - grater then or equals