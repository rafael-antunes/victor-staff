import React, { Component } from 'react';
import CoworkingAPI from '../../Models/CoworkingAPI';

import Layout from '../../Components/Layout';

import './App.css';

export default class App extends Component {
  constructor( props ) {
    super( props );
    this.coworkingApi = new CoworkingAPI();
  }

  render() {

    return (

      <div className="App">
        <Layout>
          <h1>Seja bem-vindo a mais um dia de trabalho!</h1>
        </Layout>
      </div>

    );
  }
}
