import React, { Component } from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';

import Home from './Home'
import Login from './Login'
import CadastrarCliente from './CadastrarCliente';
import ListarCliente from './ListarClientes';
import CadastrarEspaco from './CadastrarEspaco';
import ListarEspaco from './ListarEspacos';

import { RotasPrivadas } from '../Components/RotasPrivadas';


export default class Rotas extends Component {

  render() {
    return (
      <Router>
        <Route path="/login" exact component={ Login } />
        <RotasPrivadas path="/" exact component={ Home } />
        <RotasPrivadas path="/clientes/cadastrar" exact component={ CadastrarCliente } />
        <RotasPrivadas path="/clientes/listar" exact component={ ListarCliente } />
        <RotasPrivadas path="/espacos/cadastrar" exact component={ CadastrarEspaco } />
        <RotasPrivadas path="/espacos/listar" exact component={ ListarEspaco } />
      </Router>
    );
  }
}
