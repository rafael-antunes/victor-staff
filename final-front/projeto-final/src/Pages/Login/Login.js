import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import CoworkingAPI from '../../Models/CoworkingAPI';

import LoginForm from '../../Components/LoginForm'

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.coworkingApi = new CoworkingAPI();
    this.state = {
      showErrorMessage: false,
      redirect: null
    }

  }

  dadosIncorretos = () => {
    this.setState((state) => {
      return {
        ...state, showErrorMessage: true
      }
    });
  }

  redirecionar = () => {
    this.setState((state) => {
      return {
        ...state, redirect: '/'
      }
    }); 
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }
    return (
      <div className="App">
        <h1>Digite seu usuário e senha cadastrados</h1>
        <LoginForm error={this.dadosIncorretos} redirecionar={ this.redirecionar } />
        {this.state.showErrorMessage && (
          <React.Fragment>
            <h2>Não foi possível encontrar o usuário!</h2>
            <p>Tente novamente!</p>
          </React.Fragment>
        )}
      </div>

    );
  }

}
