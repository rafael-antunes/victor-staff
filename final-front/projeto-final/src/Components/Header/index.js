import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Header from '../../AntDComponents/Header';
import Menu from '../../AntDComponents/Menu';


export default class HeaderUi extends Component {

    render() {

        return (
            <React.Fragment>
                <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
                    <div className="logo" />
                    <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                        <Menu.Item key="1">{ <Link to='/login' >Element</Link>}</Menu.Item>
                        <Menu.Item key="2">nav 2</Menu.Item>
                        <Menu.Item key="3">nav 3</Menu.Item>
                    </Menu>
                </Header>
            </React.Fragment>
        )

    }
}