import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import HeaderUi from '../Header';
import { UserOutlined, AppstoreAddOutlined, NotificationOutlined } from '@ant-design/icons';
import Layout from '../../AntDComponents/Layout';
import Menu from '../../AntDComponents/Menu';


export default class MyLayout extends Component {

  render() {
    const { SubMenu } = Menu;
    const { Content, Sider } = Layout;
    const { children } = this.props;

    return (
      <Layout>
        <div style={{ height: '64px'}}><HeaderUi/></div>
        <Layout>
          <Sider width={200} className="site-layout-background">
            <Menu
              mode="inline"
              style={{ height: '100%', borderRight: 0 }}
            >
              <SubMenu key="sub1" icon={<UserOutlined />} title="Clientes">
                <Menu.Item key="1"><Link to='/clientes/cadastrar'>Cadastrar Cliente</Link></Menu.Item>
                <Menu.Item key="2"><Link to='/clientes/listar'>Clientes Cadastrados</Link></Menu.Item>
                <Menu.Item key="3">option3</Menu.Item>
                <Menu.Item key="4">option4</Menu.Item>
              </SubMenu>
              <SubMenu key="sub2" icon={<AppstoreAddOutlined />} title="Espacos">
                <Menu.Item key="5"><Link to='/espacos/cadastrar'>Cadastrar Espaço</Link></Menu.Item>
                <Menu.Item key="6"><Link to='/espacos/listar'>Listar Espaços</Link></Menu.Item>
                <Menu.Item key="7">option7</Menu.Item>
                <Menu.Item key="8">option8</Menu.Item>
              </SubMenu>
              <SubMenu key="sub3" icon={<NotificationOutlined />} title="subnav 3">
                <Menu.Item key="9">option9</Menu.Item>
                <Menu.Item key="10">option10</Menu.Item>
                <Menu.Item key="11">option11</Menu.Item>
                <Menu.Item key="12">option12</Menu.Item>
              </SubMenu>
            </Menu>
          </Sider>
          <Layout style={{ padding: '0 24px 24px' }}>
            <Content
              className="site-layout-background"
              style={{
                padding: 24,
                margin: 0,
                minHeight: 280,
                height: '100vh'
              }}
            >
              { children }
            </Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}