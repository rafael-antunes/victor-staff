import React, { Component } from 'react';

import CoworkingAPI from '../../Models/CoworkingAPI';

import Input from '../../AntDComponents/Input';
import Button from '../../AntDComponents/Button';
import Form from '../../AntDComponents/Form';

export default class LoginForm extends Component {

  efetuarLogin = (loginData) => {
    const { error, redirecionar } = this.props;
    console.log(loginData);
    const coworkingApi = new CoworkingAPI();
    coworkingApi.logarUuario({ login: loginData.usuario, senha: loginData.senha })
      .then(e => {
        console.log('kadlkasnd');
        localStorage.setItem('usuario', ({ login: loginData.usuario, senha: loginData.senha }));
        redirecionar();
      })
      .catch(e => {
        error();
      });
  }

  render() {
    return (
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 8,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={this.efetuarLogin}
      >
        <Form.Item
          label="Usuário"
          name="usuario"
          rules={[
            {
              required: true,
              message: 'Digite seu nome de usuário',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Senha"
          name="senha"
          rules={[
            {
              required: true,
              message: 'Digite sua senha!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 8,
          }}
        >
          <Button type="primary" htmlType="submit">
            Entrar
          </Button>
        </Form.Item>
      </Form>
    );
  }
};


/*

const FormularioUi = ( {isLogin} ) => {
  const [ tentativa, setTentativa ] = useState();
  const api = new Api();

  const cadastrarUsuario = ( valores ) => {
    let usuario = new CadastroModel(valores.nome, valores.email, valores.login, valores.senha);
    let requisicao = [ api.cadastrarUsuario(usuario) ];
    Promise.all( requisicao ).then( response => {
      console.log(response);
    } )
  }

  const logar = ( valores ) => {
    let login = new LoginModel(valores.login, valores.senha);
    let requisicao = [ api.autenticarUsuario( login ) ];
    Promise.all(requisicao).then( () => {
      setTentativa(tentativa + 1)
    } )
  }

  return(
    <React.Fragment>
    { localStorage.getItem("userToken") ?  <Redirect push to='/main'/> :
    <Form name="basic" labelCol={{ span: 8 }} wrapperCol={{ span: 16 }} onFinish={ isLogin ? logar : cadastrarUsuario}>
      { !isLogin && (
        <React.Fragment>
          <Form.Item label="Nome" name="nome" rules={[{ message: 'Insira seu Login' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Email" name="email" rules={[{ message: 'Insira seu Email' }]}>
            <Input />
          </Form.Item>
          </React.Fragment>
      ) }
      <Form.Item label="Login" name="login" rules={[{ required:true, message: 'Insira seu Login' }]}>
        <Input/>
      </Form.Item>

      <Form.Item label="Senha" name="senha" rules={[{ required:true, message: 'Insira sua senha!' }]}>
        <Input.Password />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="Enviar">
          Submit
        </Button>
      </Form.Item>
    </Form>
    }
    </React.Fragment>
  )
}

  export default FormularioUi;
*/
