import React, { Component } from 'react';

import Menu from '../../AntDComponents/Menu';
import Table from '../../AntDComponents/Table'

export default class ListarClientes extends Component {

  menu = (
    <Menu>
      <Menu.Item>Action 1</Menu.Item>
      <Menu.Item>Action 2</Menu.Item>
    </Menu>
  );

  NestedTable() {
    const{ clientes } = this.props;

    const expandedRowRender = recebidos => {
      const columns = [
        { title: 'Tipo de Contato', dataIndex: 'tipoContato', key: 'tipoContato' },
        { title: 'Contato', dataIndex: 'name', key: 'name' },
      ];

      const data = [];
      clientes.filter( cliente => cliente.id === recebidos.key )[0].contato.forEach( cadaContato => {
        data.push({
          tipoContato: cadaContato.tipoContato.nome,
          name: cadaContato.valor,
        });
      });
      
      return <Table columns={columns} dataSource={data} pagination={false} />;
    };

    const columns = [
      { title: 'Nome', dataIndex: 'name', key: 'name' },
      { title: 'CPF', dataIndex: 'cpf', key: 'cpf' },
      { title: 'Data de Nascimento', dataIndex: 'dataNascimento', key: 'dataNascimento' }
    ];

    const data = [];
    clientes.forEach( cliente => {
      data.push({ 
        key: cliente.id,
        name: cliente.nome,
        cpf: cliente.cpf,
        dataNascimento: cliente.dataDeNascimento
      });
    });

    return (
      <Table
        className="components-table-demo-nested"
        columns={columns}
        expandable={{ expandedRowRender }}
        dataSource={data}
      />
    );
  }

  render() {

    return (
      this.NestedTable() 
    );

  }
}
