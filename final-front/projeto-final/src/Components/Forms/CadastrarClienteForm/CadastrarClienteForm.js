import React, { Component } from 'react';
import Form from '../../../AntDComponents/Form';
import Input from '../../../AntDComponents/Input'
import Button from '../../../AntDComponents/Button'

export default class CadastrarClienteForm extends Component {  

  cadastraUsuario = ( data ) => {
    const { chamarApi } = this.props;
    chamarApi( data );
  }

  render() {

    const { buttonDisplay } = this.props;

    return (
      <>
        <Form
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          layout="vertical"
          initialValues={{
          }}
          onFinish={ this.cadastraUsuario }
        >
          <Form.Item 
            label="Nome:" 
            name="nome"
            rules={[
              {
                required: true,
                message: 'Digite seu nome do usuário',
              },
            ]}>
            <Input placeholder="Maria da Graça"/>
          </Form.Item>
          <Form.Item
          label="CPF"
          name="cpf"
          rules={[
            {
              required: true,
              message: 'Digite o CPF',
            },
          ]}
        >
          <Input placeholder="000.000.000-00" maxLength="14"/>
        </Form.Item>
        <Form.Item
          label="Data De Nascimento"
          name="dataDeNascimento"
          rules={[
            {
              required: true,
              message: 'Digite a data de nascimento',
            },
          ]}
        >
          <Input placeholder="AAAA-DD-MM" maxLength="14"/>
        </Form.Item>
        <Form.Item
          label="E-mail"
          name="email"
          rules={[
            {
              required: true,
              message: 'Digite o email',
            },
          ]}
        >
          <Input placeholder="maria@email.com"/>
        </Form.Item>        
        <Form.Item
          label="Telefone"
          name="telefone"
          rules={[
            {
              required: true,
              message: 'Digite o telefone',
            },
          ]}
        >
          <Input placeholder="(51)99999-9999"/>
        </Form.Item>
        { buttonDisplay ==='loading' && 
        <Form.Item >
          <Button type="primary" loading>Cadastrando</Button>
        </Form.Item> }

        { buttonDisplay ==='cadastrar' && 
        <Form.Item >
          <Button type="primary" htmlType="submit">Cadastrar</Button>
        </Form.Item> }

        { buttonDisplay ==='erro' && 
        <Form.Item >
          <Button type="danger" htmlType="submit">Verifique os dados</Button>
        </Form.Item> }

        { buttonDisplay ==='sucesso' && 
        <Form.Item >
          <Button type="primary" disabled>Cadastrado com sucesso</Button>
        </Form.Item> }

        </Form>
      </>
    );
  };
}





/*
import {
  Form,
  Input,
  Button,
  Radio,
  Select,
  Cascader,
  DatePicker,
  InputNumber,
  TreeSelect,
  Switch,
} from 'antd';

const FormSizeDemo = () => {
  const [componentSize, setComponentSize] = useState('default');

 

  return (
    <>
      <Form
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        initialValues={{
          size: componentSize,
        }}
        onValuesChange={onFormLayoutChange}
        size={componentSize}
      >
        <Form.Item label="Form Size" name="size">
          <Radio.Group>
            <Radio.Button value="small">Small</Radio.Button>
            <Radio.Button value="default">Default</Radio.Button>
            <Radio.Button value="large">Large</Radio.Button>
          </Radio.Group>
        </Form.Item>
        <Form.Item label="Input">
          <Input />
        </Form.Item>
        <Form.Item label="Select">
          <Select>
            <Select.Option value="demo">Demo</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item label="TreeSelect">
          <TreeSelect
            treeData={[
              {
                title: 'Light',
                value: 'light',
                children: [
                  {
                    title: 'Bamboo',
                    value: 'bamboo',
                  },
                ],
              },
            ]}
          />
        </Form.Item>
        <Form.Item label="Cascader">
          <Cascader
            options={[
              {
                value: 'zhejiang',
                label: 'Zhejiang',
                children: [
                  {
                    value: 'hangzhou',
                    label: 'Hangzhou',
                  },
                ],
              },
            ]}
          />
        </Form.Item>
        <Form.Item label="DatePicker">
          <DatePicker />
        </Form.Item>
        <Form.Item label="InputNumber">
          <InputNumber />
        </Form.Item>
        <Form.Item label="Switch">
          <Switch />
        </Form.Item>
        <Form.Item label="Button">
          <Button>Button</Button>
        </Form.Item>
      </Form>
    </>
  );
};

*/