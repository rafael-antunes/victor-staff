import axios from 'axios';



export default class CoworkingAPI {
  constructor() {
    this.endereco = 'http://localhost:8080';
    this.token = localStorage.getItem( 'bearerToken' );
    this.config = { headers: { Authorization: this.token } }
  }

  salvarUsuario( { login, senha } ) {
    return axios.post( `${this.endereco}/user/salvar`, { login, senha } );
  }

  logarUuario( { login, senha } ) {
    return axios.post( `${this.endereco}/login`, { login, senha } ).then( e => localStorage.setItem( 'bearerToken', e.headers.authorization ) );
  }

  salvarTipoContato( { object } ) {
    return axios.post( `${ this.endereco }/api/tipoContato/salvar`, { object }, this.config );
  }

  buscarTipoContato( ) {
    return axios.get( `${ this.endereco }/api/tipoContato/`, this.config );
  }

  cadastrarCliente( cliente ) {
    return axios.post( `${ this.endereco }/api/cliente/salvar`, cliente, this.config );
  }

  buscarTodosClientes() {
    return axios.get( `${ this.endereco }/api/cliente/`, this.config );
  }

  cadastrarEspaco( espaco ) {
    return axios.post( `${ this.endereco }/api/espacos/salvar`, espaco, this.config );
  }

  buscarTodosEspacos() {
    return axios.get( `${ this.endereco }/api/espacos/`, this.config ); 
  }

}
