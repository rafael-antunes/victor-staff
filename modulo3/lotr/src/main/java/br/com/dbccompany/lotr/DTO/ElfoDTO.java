package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.ElfoEntity;

public class ElfoDTO extends PersonagemDTO{

    public ElfoDTO(ElfoEntity anao) {
        super(anao);
    }

    public ElfoDTO(){
    }

    public ElfoEntity convert( ){
        ElfoEntity elfo = new ElfoEntity( this.getNome() );
        elfo.setId( super.getId() );
        elfo.setExperiencia( super.getExperiencia() );
        elfo.setVida( super.getVida() );
        elfo.setQtdDano( super.getQtdDano() );
        elfo.setQtdExperienciaPorAtaque( super.getQtdExperienciaPorAtaque() );
        elfo.setStatus(super.getStatus());
        return elfo;
    }
}