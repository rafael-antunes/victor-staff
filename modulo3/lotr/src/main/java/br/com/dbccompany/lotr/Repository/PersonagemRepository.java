package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonagemRepository<T extends PersonagemEntity> extends CrudRepository<T, Integer> {

    Optional<T> findById(Integer id);
    List<T> findAllByIdIn ( List<Integer> ids );
    T findByExperiencia ( Integer experiencia);
    List<T> findAllByExperiencia (Integer experiencia);
    List<T> findAllByExperienciaIn (List<Integer> experiencia);
    T findByNome ( String nome );
    List<T> findAllByNome ( String name );
    List<T> findAllByNomeIn ( List<String> name );
    T findByQtdDano ( Double qtdDano );
    List<T> findAllByQtdDano (Double qtdDano );
    List<T> findAllByQtdDanoIn (List<Double> qtdDano );
    T  findByQtdExperienciaPorAtaque ( Integer qtdExperienciaPorAtaque);
    List<T> findAllByQtdExperienciaPorAtaque (Integer qtdExperienciaPorAtaque);
    List<T> findAllByQtdExperienciaPorAtaqueIn (List<Integer> qtdExperienciaPorAtaque);
    T findByStatus ( String status );
    List<T> findAllByStatus ( String status );
    List<T> findAllByStatusIn ( List<String> status );
    T findByVida( Double vida );
    List<T> findAllByVida ( Double  vida );
    List<T> findAllByVidaIn ( List<Double>  vida );
    //T findByIdInventario(Integer idInventario);

}
