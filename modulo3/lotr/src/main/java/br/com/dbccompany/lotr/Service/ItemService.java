package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ErroDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Exception.ArgumentosInvelidosItem;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    private ItemRepository repository;

    private Logger logger = LoggerFactory.getLogger(LotrApplication.class);

    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8081/logger/salvar";

    public List<ItemDTO> trazerTodosOsItens(){
        return this.convertList( repository.findAll() );
    }

    @Transactional( rollbackFor = Exception.class)
    public ItemDTO save( ItemDTO item ) throws ArgumentosInvelidosItem {
        return this.saveAndEdit( item.converter() );
    }

    @Transactional( rollbackFor = Exception.class)
    public ItemDTO edit( ItemDTO item, Integer id ) throws ArgumentosInvelidosItem {
        ItemEntity itemEntity = item.converter();
        itemEntity.setId(id);
        return this.saveAndEdit( itemEntity );
    }

    private ItemDTO saveAndEdit( ItemEntity item ) throws ArgumentosInvelidosItem {
        ItemEntity itemNovo = repository.save( item );
        return new ItemDTO(itemNovo);
    }

    public ItemDTO buscarPorId( Integer id) throws ItemNaoEncontrado {
        try {
            Optional<ItemEntity> item = repository.findById(id);
            return new ItemDTO(repository.findById(id).get());
        }catch(Exception e){
            String mensagemLogger = "Erro Ao realizar operação!";
            logger.error(mensagemLogger);
            ErroDTO erro = new ErroDTO( e, "ERROR", mensagemLogger, "400" );
            restTemplate.postForObject(url, erro, Object.class);
            throw new ItemNaoEncontrado();
        }
    }

    public List<ItemDTO> findAllByIdIn(List<Integer> id){
        return this.convertList( repository.findAllByIdIn( id ) );
    }

    public ItemDTO findByDescricao( String descricao ){
        return new ItemDTO(repository.findByDescricao( descricao));
    };

    public List<ItemDTO> findAllByDescricao( String descricao ){
        return this.convertList( repository.findAllByDescricao( descricao ) );
    }

    public List<ItemDTO> findAllByDescricaoIn( List<String> descricao ){
        return this.convertList( repository.findAllByDescricaoIn( descricao ) );
    }

    private List<ItemDTO> convertList( List<ItemEntity> entidades){
        ArrayList<ItemDTO> itensNovo = new ArrayList<ItemDTO>();
        for( ItemEntity entidade:entidades){
            itensNovo.add( new ItemDTO( entidade ) );
        }
        return itensNovo;
    }

}
