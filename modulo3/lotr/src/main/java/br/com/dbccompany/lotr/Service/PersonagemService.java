package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.PersonagemDTO;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import br.com.dbccompany.lotr.Repository.PersonagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public abstract class PersonagemService < R extends PersonagemRepository<E>, E extends PersonagemEntity>{

    @Autowired
    private R repository;

    public List<E> trazerTodosOsPersonagens(){
        return (List<E>) repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class)
    public E save( E item ){
        return this.saveAndEdit( item );
    }

    @Transactional( rollbackFor = Exception.class)
    public E edit( E item, Integer id ){
        item.setId(id);
        return this.saveAndEdit( item );
    }

    private E saveAndEdit( E item ){
        return repository.save( item );
    }

    public E buscarPorId( Integer id){
        Optional<E> item = repository.findById(id);
        if( item.isPresent() ){
            return repository.findById(id).get();
        }
        return null;
    }

    public List<E> findAllByIdIn ( List<Integer> ids ){
        return repository.findAllByIdIn( ids );
    }

    public E findByExperiencia ( Integer experiencia ){
        return repository.findByExperiencia( experiencia );
    }

    public List<E> findAllByExperiencia ( Integer experiencia ){
        return repository.findAllByExperiencia( experiencia );
    }

    public List<E> findAllByExperienciaIn ( List<Integer> experiencia ){
        return repository.findAllByExperienciaIn( experiencia );
    }

    public E findByNome ( String nome ){
        return repository.findByNome( nome );
    }
    public List<E> findAllByNome ( String name ){
        return repository.findAllByNome( name );
    }

    public List<E> findAllByNomeIn ( List<String> name ){
        return repository.findAllByNomeIn( name );
    }

    public E findByQtdDano ( Double qtdDano ){
        return repository.findByQtdDano( qtdDano);
    }

    public List<E> findAllByQtdDano (Double qtdDano ){
        return repository.findAllByQtdDano( qtdDano );
    }

    public List<E> findAllByQtdDanoIn (List<Double> qtdDano ){
        return repository.findAllByQtdDanoIn( qtdDano );
    }

    public E  findByQtdExperienciaPorAtaque ( Integer qtdExperienciaPorAtaque){
        return repository.findByQtdExperienciaPorAtaque( qtdExperienciaPorAtaque );
    }

    public List<E> findAllByQtdExperienciaPorAtaque (Integer qtdExperienciaPorAtaque){
        return repository.findAllByQtdExperienciaPorAtaque( qtdExperienciaPorAtaque );
    }

    public List<E> findAllByQtdExperienciaPorAtaqueIn (List<Integer> qtdExperienciaPorAtaque){
        return repository.findAllByQtdExperienciaPorAtaqueIn( qtdExperienciaPorAtaque );
    }

    public E findByStatus ( String status ){
        return repository.findByStatus( status );
    }

    public List<E> findAllByStatus ( String status ){
        return repository.findAllByStatus( status );
    }
    public List<E> findAllByStatusIn ( List<String> status ){
        return repository.findAllByStatusIn( status );
    }

    public E findByVida( Double vida ){
        return repository.findByVida( vida );
    }

    public List<E> findAllByVida ( Double  vida ){
        return repository.findAllByVida( vida );
    }
    public List<E> findAllByVidaIn ( List<Double>  vida ){
        return repository.findAllByVidaIn( vida );
    }

}