package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InventarioRepository extends CrudRepository<InventarioEntity, Integer> {

    Optional<InventarioEntity> findById (Integer id);
    List<InventarioEntity> findAllByIdIn (List<Integer> ids);
    InventarioEntity findByPersonagem ( PersonagemEntity personagem);
    List<InventarioEntity> findAllByPersonagem ( PersonagemEntity personagem );
    List<InventarioEntity> findAllByPersonagemIn (List<PersonagemEntity> personagem);
}
