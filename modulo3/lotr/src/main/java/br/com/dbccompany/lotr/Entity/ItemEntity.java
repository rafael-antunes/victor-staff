package br.com.dbccompany.lotr.Entity;


import javax.persistence.*;
import java.util.List;

@Entity
public class ItemEntity {

    @Id
    @SequenceGenerator( name = "ITEM_SEQ", sequenceName = "ITEM_SEQ")
    @GeneratedValue( generator= "ITEM_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( nullable = false)
    private String descricao;

    @OneToMany( mappedBy = "item")
    private List<Inventario_X_ItemEntity> InventarioItem;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Inventario_X_ItemEntity> getInventarioItem() {
        return InventarioItem;
    }

    public void setInventarioItem(List<Inventario_X_ItemEntity> inventarioItem) {
        InventarioItem = inventarioItem;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
