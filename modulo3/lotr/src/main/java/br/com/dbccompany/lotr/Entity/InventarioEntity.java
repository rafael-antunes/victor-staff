package br.com.dbccompany.lotr.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class InventarioEntity {

    @Id
    @SequenceGenerator( name = "INVENTARIO_SEQ", sequenceName = "INVENTARIO_SEQ")
    @GeneratedValue( generator= "INVENTARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @OneToMany( mappedBy = "inventario")
    private List<Inventario_X_ItemEntity> inventarioItem;

    @OneToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_personagem")
    private PersonagemEntity personagem;

    public List<Inventario_X_ItemEntity> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<Inventario_X_ItemEntity> inventarioItem) {
        inventarioItem = inventarioItem;
    }

    public PersonagemEntity getPersonagem() {
        return personagem;
    }

    public void setPersonagem(PersonagemEntity personagem) {
        this.personagem = personagem;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}