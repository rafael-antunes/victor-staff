package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface Inventario_X_ItemRepository extends CrudRepository<Inventario_X_ItemEntity, Inventario_X_ItemId> {

    Optional<Inventario_X_ItemEntity> findById( Inventario_X_ItemId id );
    List<Inventario_X_ItemEntity> findAllByIdIn( List<Inventario_X_ItemId> id );
    Inventario_X_ItemEntity findByInventario( Integer idInventario );
    List<Inventario_X_ItemEntity> findAllByInventario( Integer idInventario );
    List<Inventario_X_ItemEntity> findAllByInventarioIn( List<Integer> idsInventario );
    Inventario_X_ItemEntity findByItem( Integer idItem );
    List<Inventario_X_ItemEntity> findAllByItem( Integer idItem );
    List<Inventario_X_ItemEntity> findAllByItemIn( List<Integer> idsItem );

}
