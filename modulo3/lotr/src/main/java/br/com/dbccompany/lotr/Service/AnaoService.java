package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Repository.AnaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnaoService extends PersonagemService<AnaoRepository, AnaoEntity> {

    public List<AnaoDTO> trazerTodosOsItensAnao(){
        return this.convertList( super.trazerTodosOsPersonagens() );
    }

    public AnaoDTO saveAnao( AnaoDTO anao ){
        return new AnaoDTO( super.save( anao.convert() ) );
    }

    public AnaoDTO editAnao ( AnaoDTO anao, Integer id ){
        return new AnaoDTO( super.edit( anao.convert(), id ));
    }

    public AnaoDTO buscarAnaoPorId( Integer id ){
        return new AnaoDTO( super.buscarPorId( id ) );
    }

    public List<AnaoDTO> buscarTodosAnoesPorId( List<Integer> ids ){
        return this.convertList( findAllByIdIn( ids ) );
    }

    public AnaoDTO buscarAnaoPorExperiencia( Integer id ){
        return new AnaoDTO( super.findByExperiencia( id ) );
    }

    public List<AnaoDTO> buscarTodosAnoesPorExperiencia( Integer id ){
        return this.convertList( findAllByExperiencia( id ) );
    }

    public List<AnaoDTO> buscarTodosAnoesPorExperiencia( List<Integer> ids ){
        return this.convertList( findAllByExperienciaIn( ids ) );
    }

    public AnaoDTO buscarAnaoPorNome( String nome ){
        return new AnaoDTO( super.findByNome( nome ) );
    }

    public List<AnaoDTO> buscarTodosAnoesPorNome( String nome ){
        return this.convertList( findAllByNome( nome ) );
    }

    public List<AnaoDTO> buscarTodosAnoesPorNome( List<String> nomes ){
        return this.convertList( findAllByNomeIn( nomes ) );
    }

    public AnaoDTO buscarAnaoPorQtdDano ( Double qtdDano ){
        return new AnaoDTO( super.findByQtdDano( qtdDano ) );
    }

    public List<AnaoDTO> buscarTodosAnoesQtdDano ( Double qtdDano ){
        return this.convertList( findAllByQtdDano( qtdDano ) );
    }

    public List<AnaoDTO> buscarTodosAnoesQtdDano ( List<Double> qtdDano ){
        return this.convertList( findAllByQtdDanoIn( qtdDano ) );
    }

    public AnaoDTO buscarAnaoPorQtdExperienciaPorAtaque ( Integer qtdExperienciaPorAtaque){
        return new AnaoDTO( super.findByQtdExperienciaPorAtaque( qtdExperienciaPorAtaque ) );
    }

    public List<AnaoDTO> buscarTodosAnoesQtdExperienciaPorAtaque (Integer qtdExperienciaPorAtaque){
        return this.convertList( findAllByQtdExperienciaPorAtaque (qtdExperienciaPorAtaque) );
    }

    public List<AnaoDTO> buscarTodosAnoesQtdExperienciaPorAtaque (List<Integer> qtdExperienciaPorAtaque){
        return this.convertList( findAllByQtdExperienciaPorAtaqueIn ( qtdExperienciaPorAtaque ) );
    }

    private List<AnaoDTO> convertList ( List<AnaoEntity> entidades){
        ArrayList<AnaoDTO> anoesConvertidos = new ArrayList<>();
        for ( AnaoEntity entidade:entidades){
            anoesConvertidos.add( new AnaoDTO( entidade ) );
        }
        return anoesConvertidos;
    }
/*
    public E findByStatus ( String status ){
        return repository.findByStatus( status );
    }

    public List<E> findAllByStatus ( String status ){
        return repository.findAllByStatus( status );
    }
    public List<E> findAllByStatusIn ( List<String> status ){
        return repository.findAllByStatusIn( status );
    }

    public E findByVida( Double vida ){
        return repository.findByVida( vida );
    }

    public List<E> findAllByVida ( Double  vida ){
        return repository.findAllByVida( vida );
    }
    public List<E> findAllByVidaIn ( List<Double>  vida ){
        return repository.findAllByVidaIn( vida );
    }

 */


}
