package br.com.dbccompany.lotr.Controller;


import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Service.Inventario_X_ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/inventario_x_item" )
public class Inventario_X_ItemController {

    @Autowired
    private Inventario_X_ItemService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> trazerTodosInventarios_X_Item(){
        return service.trazerTodosOsItens();
    }

    @GetMapping( value = "/{ id }" )
    @ResponseBody
    public Inventario_X_ItemDTO trazerItemEspecifico(@PathVariable Inventario_X_ItemId id ){
        return service.buscarPorId( id );
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public Inventario_X_ItemDTO salvarItem(@RequestBody Inventario_X_ItemDTO inventario ){
        return service.save( inventario );
    }

    @PutMapping ( value = "/editar/ { id }")
    @ResponseBody
    public Inventario_X_ItemDTO editarItem( @RequestBody Inventario_X_ItemDTO inventario, @PathVariable Inventario_X_ItemId id ){
        return service.edit( inventario, id );
    }

    @GetMapping ( value = "/encontrarTodosPorId" )
    @ResponseBody
    public List<Inventario_X_ItemDTO> encontrarTodosPorId ( @RequestBody List<Inventario_X_ItemId> ids) {
        return service.findAllByIdIn( ids );
    }

}