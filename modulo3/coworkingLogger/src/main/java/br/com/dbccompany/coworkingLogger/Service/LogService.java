package br.com.dbccompany.coworkingLogger.Service;

import br.com.dbccompany.coworkingLogger.Collection.Log;
import br.com.dbccompany.coworkingLogger.DTO.LogDTO;
import br.com.dbccompany.coworkingLogger.Exception.ArgumentosInvalidos;
import br.com.dbccompany.coworkingLogger.Exception.LogNaoEncontrado;
import br.com.dbccompany.coworkingLogger.Repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogService {

    @Autowired
    private LogRepository repository;

    private MongoTemplate mongoTemplate;

    public List<LogDTO> findAll(){
        return this.convertListToDTO( repository.findAll() );
    }

    public LogDTO insert(LogDTO erroDTO) throws ArgumentosInvalidos {
        try{
            Log erroLog = erroDTO.convert();
            return new LogDTO( repository.insert(erroLog) );
        }catch(Exception e){
            System.err.println("Erro ao inserir");
            throw new ArgumentosInvalidos();
        }
    }

    public List<LogDTO> buscarPorCodigo(String codigo) throws LogNaoEncontrado {
        try{
            return this.convertListToDTO( repository.findAllByCodigo(codigo) );
        }catch(Exception e){
            System.err.println("Erro ao realizar busca");
            throw new LogNaoEncontrado();
        }
    }

    public List<LogDTO> buscarPorTipo( String tipo ) throws LogNaoEncontrado{
        try{
            return this.convertListToDTO( repository.findAllByTipo( tipo ) );
        }catch(Exception e){
            System.err.println("Erro ao realizar busca");
            throw new LogNaoEncontrado();
        }
    }

    private List<LogDTO> convertListToDTO(List<Log> entidades){
        ArrayList<LogDTO> itensNovo = new ArrayList<LogDTO>();
        for( Log entidade:entidades){
            itensNovo.add( new LogDTO( entidade ) );
        }
        return itensNovo;
    }

}
