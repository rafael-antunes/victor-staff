package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.Tipo_ContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
@Profile("test")
public class ClienteTest {

    @Autowired
    private ClientesRepository repository;

    @Test
    public void salvarCliente(){
        ClienteEntity cliente = new ClienteEntity();
        cliente.setCpf("013.149.860-66");
        cliente.setNome("Josias");
        cliente.setDataDeNascimento( LocalDate.parse("1997-11-04") );
        repository.save( cliente );
        assertEquals( cliente.getNome(), (repository.findByNome("Josias")).getNome() );
    }
    @Test
    public void salvarERecebeCPFCorretamente(){
        ClienteEntity cliente = new ClienteEntity();
        cliente.setCpf("013.149.860-66");
        cliente.setNome("Maria");
        cliente.setDataDeNascimento( LocalDate.parse("1997-11-04") );
        repository.save( cliente );
        assertEquals( cliente.getCpf(), (repository.findByNome("Maria")).getCpf() );
    }

    @Test
    public void clienteNaoExistente(){
        String nome = "abacaxiVoador";
        assertNull( repository.findByNome( nome ));
    }

}
