package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class PacoteTest {

    @Autowired
    private PacoteRepository repository;

    @Test
    public void SalvaPacote(){
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(55.20);
        pacote = repository.save(pacote);
        Optional<PacoteEntity> pacoteSalvo = repository.findById(pacote.getId());
        assertEquals(pacote.getId(), pacoteSalvo.get().getId());
    }


}
