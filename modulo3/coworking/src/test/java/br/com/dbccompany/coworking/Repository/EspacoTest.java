package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class EspacoTest {

    @Autowired
    private EspacosRepository repository;

    @Test
    public void salvarEspaco(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setValor(75.0);
        espaco.setNome("Espaco1");
        espaco.setQtdPessoas(19);
        repository.save(espaco);
        assertEquals(espaco.getNome(), repository.findByNome(espaco.getNome()).getNome());
    }

}
