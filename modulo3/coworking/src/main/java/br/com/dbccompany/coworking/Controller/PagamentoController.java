package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.DTO.PagamentoDTO;
import br.com.dbccompany.coworking.DTO.PagamentoDTOResponse;
import br.com.dbccompany.coworking.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/pagamento" )
public class PagamentoController {

    @Autowired
    private PagamentoService service;

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<PagamentoDTOResponse> trazerElementoEspecifico(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>( service.findById(id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<PagamentoDTOResponse>> trazerTodos() {
        try {
            return new ResponseEntity<>(service.trazerTodos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/pagar")
    @ResponseBody
    public ResponseEntity<PagamentoDTOResponse> realizarPagamento(@RequestBody PagamentoDTO pacote) {
        try {
            return new ResponseEntity<>( service.realizaPagamento(pacote), HttpStatus.OK  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }
}
