package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Cliente_PacoteEntity;
import br.com.dbccompany.coworking.Entity.Espaco_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;

import java.util.ArrayList;
import java.util.List;

public class PacoteDTORetorno {
    private Integer id;
    private Double valor;
    private List<Espaco_PacoteDTORetorno> espacoPacote;
    private List<Cliente_PacoteDTOResponse> clientePacote;

    public PacoteDTORetorno(){}

    public PacoteDTORetorno(PacoteEntity entity) {
        this.id = entity.getId();
        this.valor = entity.getValor();
        this.espacoPacote = this.convertEntityToDTO( entity.getEspacoPacote() );
        this.clientePacote = this.convertClientesEntityToDTO( entity.getClientePacote() );
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<Espaco_PacoteDTORetorno> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote(ArrayList<Espaco_PacoteDTORetorno> espacoPacote) {
        this.espacoPacote = espacoPacote;
    }

    public void setEspacoPacote(List<Espaco_PacoteDTORetorno> espacoPacote) {
        this.espacoPacote = espacoPacote;
    }

    public List<Cliente_PacoteDTOResponse> getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(List<Cliente_PacoteDTOResponse> clientePacote) {
        this.clientePacote = clientePacote;
    }

    private List<Espaco_PacoteDTORetorno> convertEntityToDTO (List<Espaco_PacoteEntity> arrayEntity){
        ArrayList<Espaco_PacoteDTORetorno> arrayDTO = new ArrayList<Espaco_PacoteDTORetorno>();
        if(arrayEntity == null ){
            return null;
        }
        for( Espaco_PacoteEntity entidade:arrayEntity){
            arrayDTO.add( new Espaco_PacoteDTORetorno( entidade ) );
        }
        return arrayDTO;
    }

    private List<Cliente_PacoteDTOResponse> convertClientesEntityToDTO ( List<Cliente_PacoteEntity> arrayEntity){
        ArrayList<Cliente_PacoteDTOResponse> arrayDTO = new ArrayList<Cliente_PacoteDTOResponse>();
        if(arrayEntity == null ){
            return null;
        }
        for( Cliente_PacoteEntity entidade:arrayEntity){
            arrayDTO.add( new Cliente_PacoteDTOResponse( entidade ) );
        }
        return arrayDTO;
    }

}
