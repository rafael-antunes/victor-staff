package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.DTO.PacoteDTORetorno;
import br.com.dbccompany.coworking.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/pacote")
public class PacoteController {

    @Autowired
    private PacoteService service;

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<PacoteDTO> trazerElementoEspecifico(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>( service.findById(id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<PacoteDTORetorno>> trazerTodosElementos() {
        try {
            return new ResponseEntity<>(service.trazerTodosPacotes(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<PacoteDTO> salvarItem(@RequestBody PacoteDTO pacote) {
        try {
            return new ResponseEntity<>( service.save(pacote), HttpStatus.OK  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<PacoteDTO> editarItem(@RequestBody PacoteDTO pacote, @PathVariable Integer id) {
        try {
            return new ResponseEntity<>( service.edit(pacote, id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}