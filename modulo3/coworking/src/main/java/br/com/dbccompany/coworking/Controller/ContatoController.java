package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/contato" )
public class ContatoController {

    @Autowired
    private ContatoService service;

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ResponseEntity<ContatoDTO> trazerItemEspecifico(@PathVariable Integer id ){
        try {
            return new ResponseEntity<>( service.findById(id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>( null , HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping( value = "/" )
    @ResponseBody
    public ResponseEntity<List<ContatoDTO>> trazerTodosItens(){
        try {
            return new ResponseEntity<>(service.trazerTodosOsItens(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<ContatoDTO> salvarItem(@RequestBody ContatoDTO contato ){
        try {
            return new ResponseEntity<>( service.save(contato), HttpStatus.OK  );
        }catch( Exception e ){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

    @PutMapping ( value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<ContatoDTO> editarItem( @RequestBody ContatoDTO contato, @PathVariable Integer id){
        try {
            return new ResponseEntity<>( service.edit(contato, id), HttpStatus.OK );
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}