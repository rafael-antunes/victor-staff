package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.DTO.ContratacaoDTOResponse;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Exception.ErroAoRealizarOperacao;
import br.com.dbccompany.coworking.Exception.ErroAoSalvar;
import br.com.dbccompany.coworking.Exception.RetornoDeBuscaInvalido;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository repository;

    @Autowired
    private EspacosRepository espacoRepository;

    @Autowired
    private ClientesRepository clienteRepository;

    public List<ContratacaoDTOResponse> trazerTodosOsItens() throws RetornoDeBuscaInvalido {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public ContratacaoDTOResponse save( ContratacaoDTO contratacaoDTO ) throws ErroAoSalvar {
        try {
            ContratacaoEntity contratacao = contratacaoDTO.convert();
            contratacao.setEspaco( espacoRepository.findById( contratacaoDTO.getEspaco().getId() ).get() );
            contratacao.setCliente( clienteRepository.findById( contratacaoDTO.getCliente().getId() ).get() );
            return this.saveAndEdit( contratacao );
        } catch (Exception e) {
            Log.enviaErro400(e);
            throw new ErroAoSalvar();
        }
    }

    private ContratacaoDTOResponse saveAndEdit( ContratacaoEntity contratacao ){
        contratacao = repository.save( contratacao );
        return new ContratacaoDTOResponse( contratacao );
    }

    public ContratacaoDTOResponse findById( Integer id) throws RetornoDeBuscaInvalido {
        try {
            Optional<ContratacaoEntity> contratacao = repository.findById(id);
            return new ContratacaoDTOResponse(repository.findById(id).get());
        }catch( Exception e ){
            Log.enviaErro404(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    private List<ContratacaoDTOResponse> convertList( Iterable<ContratacaoEntity> entidades ){
        ArrayList<ContratacaoDTOResponse> contratacaoConvertidos = new ArrayList<ContratacaoDTOResponse>();
        for( ContratacaoEntity entidade:entidades){
            contratacaoConvertidos.add( new ContratacaoDTOResponse( entidade ) );
        }
        return contratacaoConvertidos;
    }

}
