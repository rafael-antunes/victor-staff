package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class EspacoEntity {

    @Id
    @SequenceGenerator( name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue( generator= "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( nullable = false, unique = true )
    private String nome;

    @Column( nullable = false )
    private Integer qtdPessoas;

    @Column( nullable = false )
    private Double valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
