package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContratacaoDTOResponse;
import br.com.dbccompany.coworking.DTO.Espaco_PacoteDTO;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Espaco_PacoteEntity;
import br.com.dbccompany.coworking.Exception.ErroAoRealizarOperacao;
import br.com.dbccompany.coworking.Exception.ErroAoSalvar;
import br.com.dbccompany.coworking.Exception.RetornoDeBuscaInvalido;
import br.com.dbccompany.coworking.Repository.Espaco_PacoteRepository;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Espaco_PacoteService {

    @Autowired
    private Espaco_PacoteRepository repository;

    @Autowired
    private PacoteRepository pacoteRepository;

    @Autowired
    private EspacosRepository espacoRepository;

    public List<Espaco_PacoteDTO> trazerTodosEspacoPacote() throws RetornoDeBuscaInvalido {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public Espaco_PacoteDTO save( Espaco_PacoteDTO espacoPacoteDTO ) throws ErroAoSalvar {
        try {
            Espaco_PacoteEntity espacoPacote = espacoPacoteDTO.convert();
            espacoPacote.setPacote( pacoteRepository.findById( espacoPacoteDTO.getPacote().getId() ).get() );
            espacoPacote.setEspaco( espacoRepository.findById( espacoPacoteDTO.getEspaco().getId() ).get() );
            return this.saveAndEdit(espacoPacote);
        } catch (Exception e) {
            Log.enviaErro400(e);
            throw new ErroAoSalvar();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public Espaco_PacoteDTO edit( Espaco_PacoteDTO espacoPacote, Integer id ) throws ErroAoRealizarOperacao {
        try{
            Espaco_PacoteEntity espacoPacoteEntity = espacoPacote.convert();
            espacoPacoteEntity.setId(id);
            return this.saveAndEdit( espacoPacoteEntity );
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new ErroAoRealizarOperacao();
        }
    }

    private Espaco_PacoteDTO saveAndEdit( Espaco_PacoteEntity espacoPacote ){
        Espaco_PacoteEntity espacoPacoteNovo = repository.save( espacoPacote );
        return new Espaco_PacoteDTO( espacoPacoteNovo );
    }

    public Espaco_PacoteDTO findById( Integer id) throws RetornoDeBuscaInvalido {
        try {
            Optional<Espaco_PacoteEntity> espaco_Pacote = repository.findById(id);
            if (espaco_Pacote.isPresent()) {
                return new Espaco_PacoteDTO(repository.findById(id).get());
            }
            return null;
        }catch( Exception e ){
            Log.enviaErro404(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    private List<Espaco_PacoteDTO> convertList( Iterable<Espaco_PacoteEntity> entidades ){
        ArrayList<Espaco_PacoteDTO> espacoPacoteConvertidos = new ArrayList<Espaco_PacoteDTO>();
        for( Espaco_PacoteEntity entidade:entidades){
            espacoPacoteConvertidos.add( new Espaco_PacoteDTO( entidade ) );
        }
        return espacoPacoteConvertidos;
    }
}
