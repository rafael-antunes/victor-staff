package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContratacaoDTOResponse;
import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.DTO.PacoteDTORetorno;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Exception.ErroAoRealizarOperacao;
import br.com.dbccompany.coworking.Exception.ErroAoSalvar;
import br.com.dbccompany.coworking.Exception.RetornoDeBuscaInvalido;
import br.com.dbccompany.coworking.Repository.Cliente_PacoteRepository;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PacoteService {


    @Autowired
    private PacoteRepository repository;
    @Autowired
    Cliente_PacoteRepository clientePacoteRepository;

    public List<PacoteDTORetorno> trazerTodosPacotes() throws RetornoDeBuscaInvalido {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public PacoteDTO save( PacoteDTO pacote ) throws ErroAoSalvar {
        try {
            return this.saveAndEdit(pacote.convert());
        } catch (Exception e) {
            Log.enviaErro400(e);
            throw new ErroAoSalvar();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public PacoteDTO edit( PacoteDTO pacote, Integer id ) throws ErroAoRealizarOperacao {
        try {
            PacoteEntity pacoteEntity = pacote.convert();
            pacoteEntity.setId(id);
            return this.saveAndEdit( pacoteEntity );
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new ErroAoRealizarOperacao();
        }
    }

    private PacoteDTO saveAndEdit( PacoteEntity pacote ){
        PacoteEntity pacoteNovo = repository.save( pacote );
        return new PacoteDTO( pacoteNovo );
    }

    public PacoteDTO findById( Integer id) throws RetornoDeBuscaInvalido {
        try {
            Optional<PacoteEntity> contratacao = repository.findById(id);
            if (contratacao.isPresent()) {
                return new PacoteDTO(repository.findById(id).get());
            }
            return null;
        }catch( Exception e ){
            Log.enviaErro404(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    private List<PacoteDTORetorno> convertList(Iterable<PacoteEntity> entidades ){
        ArrayList<PacoteDTORetorno> pacotesConvertidos = new ArrayList<PacoteDTORetorno>();
        for( PacoteEntity entidade:entidades){
            pacotesConvertidos.add( new PacoteDTORetorno( entidade ) );
        }
        return pacotesConvertidos;
    }

}
