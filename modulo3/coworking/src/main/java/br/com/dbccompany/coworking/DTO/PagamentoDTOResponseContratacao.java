package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.PagamentoEntity;

public class PagamentoDTOResponseContratacao extends PagamentoDTOResponse {
    private ContratacaoDTOResponse contratacao;

    public PagamentoDTOResponseContratacao(PagamentoEntity entity){
        super( entity );
        this.contratacao = new ContratacaoDTOResponse( entity.getContratacao() );
    }

    public ContratacaoDTOResponse getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoDTOResponse contratacao) {
        this.contratacao = contratacao;
    }
}
