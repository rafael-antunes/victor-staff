package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.DTO.SaldoClienteDTOResponse;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.ErroAoRealizarOperacao;
import br.com.dbccompany.coworking.Exception.ErroAoSalvar;
import br.com.dbccompany.coworking.Exception.RetornoDeBuscaInvalido;
import br.com.dbccompany.coworking.Repository.SaldoClienteRespository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRespository repository;

    @Transactional(rollbackFor = Exception.class)
    public SaldoClienteDTOResponse pagarContratacao (ContratacaoEntity contratacao) {
        SaldoClienteId id = new SaldoClienteId();
        id.setId_cliente(contratacao.getEspaco().getId());
        id.setId_cliente(contratacao.getCliente().getId());
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        if (repository.findById(id).isPresent()) {
            saldoCliente = repository.findById(id).get();
            saldoCliente.setQuantidade(saldoCliente.getQuantidade() + contratacao.getQuantidade());
            saldoCliente.setVencimento(saldoCliente.getVencimento().plusDays((Integer) contratacao.getPrazo()));
        } else {
            saldoCliente.setId(id);
            saldoCliente.setCliente(contratacao.getCliente());
            saldoCliente.setEspaco(contratacao.getEspaco());
            saldoCliente.setTipoContratacao(contratacao.getTipoContratacao());
            saldoCliente.setQuantidade(contratacao.getQuantidade());
            LocalDate vencimento = LocalDate.now().plusDays((Integer) contratacao.getPrazo());
            saldoCliente.setVencimento(vencimento);
        }
        return new SaldoClienteDTOResponse(repository.save(saldoCliente));
    }

    @Transactional( rollbackFor = Exception.class)
    public List<SaldoClienteDTOResponse> pagarClientePacote( Cliente_PacoteEntity clientePacote ){
        List<SaldoClienteDTOResponse> saldosCliente = new ArrayList<>();
        Integer quantidade = clientePacote.getQuantidade();
        ClienteEntity cliente = clientePacote.getCliente();
        List<Espaco_PacoteEntity> espacoPacotes = clientePacote.getPacote().getEspacoPacote();
        for(Espaco_PacoteEntity espacoPacote : espacoPacotes){
            SaldoClienteId id = new SaldoClienteId();
            id.setId_cliente(cliente.getId());
            id.setId_espaco(espacoPacote.getEspaco().getId());
            SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
            if( repository.findById(id).isPresent() ){
                saldoCliente = repository.findById(id).get();
                saldoCliente.setQuantidade(saldoCliente.getQuantidade() + (quantidade * espacoPacote.getQuantidade()));
                saldoCliente.setVencimento(saldoCliente.getVencimento().plusDays((Integer)(quantidade * espacoPacote.getPrazo())));
            }
            else{
                saldoCliente.setId( id );
                saldoCliente.setCliente( cliente );
                saldoCliente.setEspaco( espacoPacote.getEspaco() );
                saldoCliente.setTipoContratacao(espacoPacote.getTipoContratacao());
                saldoCliente.setQuantidade(quantidade * espacoPacote.getQuantidade());
                saldoCliente.setVencimento(LocalDate.now().plusDays((Integer)(quantidade * espacoPacote.getPrazo())));
            }
            saldosCliente.add( new SaldoClienteDTOResponse( repository.save(saldoCliente) ));
        }
        return saldosCliente;
    }

    public List<SaldoClienteDTOResponse> trazerTodos() throws RetornoDeBuscaInvalido {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public SaldoClienteDTOResponse save(SaldoClienteDTO saldoCliente ) throws ErroAoSalvar {
        try {
            return this.saveAndEdit( saldoCliente.convert() );
        } catch (Exception e) {
            Log.enviaErro400(e);
            throw new ErroAoSalvar();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public SaldoClienteDTOResponse edit(SaldoClienteDTO saldoCliente, SaldoClienteId id ) throws ErroAoRealizarOperacao {
        try {
            SaldoClienteEntity saldoClienteEntity = saldoCliente.convert();
            saldoClienteEntity.setId(id);
            return this.saveAndEdit( saldoClienteEntity );
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new ErroAoRealizarOperacao();
        }
    }

    private SaldoClienteDTOResponse saveAndEdit( SaldoClienteEntity saldoCliente ){
        SaldoClienteEntity espacoNovo = repository.save( saldoCliente );
        return new SaldoClienteDTOResponse( espacoNovo );
    }

    public SaldoClienteDTOResponse findById(SaldoClienteId id) throws RetornoDeBuscaInvalido {
        try {
            Optional<SaldoClienteEntity> espaco = repository.findById(id);
            return new SaldoClienteDTOResponse(repository.findById(id).get());
        }catch( Exception e ){
            Log.enviaErro404(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    private List<SaldoClienteDTOResponse> convertList(Iterable<SaldoClienteEntity> entidades ){
        ArrayList<SaldoClienteDTOResponse> listaConvertida = new ArrayList<SaldoClienteDTOResponse>();
        for( SaldoClienteEntity entidade:entidades){
            listaConvertida.add( new SaldoClienteDTOResponse( entidade ) );
        }
        return listaConvertida;
    }

}
