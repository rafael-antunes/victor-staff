package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EspacosRepository extends CrudRepository<EspacoEntity, Integer > {

    @Override
    Optional<EspacoEntity> findById(Integer id);
    EspacoEntity findByNome( String nome );
}
