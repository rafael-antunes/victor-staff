package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContratacaoDTOResponse;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Exception.ErroAoRealizarOperacao;
import br.com.dbccompany.coworking.Exception.ErroAoSalvar;
import br.com.dbccompany.coworking.Exception.RetornoDeBuscaInvalido;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EspacosService {

    @Autowired
    private EspacosRepository repository;

    public List<EspacoDTO> trazerTodosEspacos() throws RetornoDeBuscaInvalido {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public EspacoDTO save(EspacoDTO espaco ) throws ErroAoSalvar {
        try {
            return this.saveAndEdit( espaco.convert() );
        } catch (Exception e) {
            Log.enviaErro400(e);
            throw new ErroAoSalvar();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public EspacoDTO edit(EspacoDTO espaco, Integer id ) throws ErroAoRealizarOperacao {
        try {
            EspacoEntity espacoEntity = espaco.convert();
            espacoEntity.setId(id);
            return this.saveAndEdit( espacoEntity );
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new ErroAoRealizarOperacao();
        }
    }

    private EspacoDTO saveAndEdit(EspacoEntity espaco ){
        EspacoEntity espacoNovo = repository.save( espaco );
        return new EspacoDTO( espacoNovo );
    }

    public EspacoDTO findById(Integer id) throws RetornoDeBuscaInvalido {
        try {
            Optional<EspacoEntity> espaco = repository.findById(id);
            if (espaco.isPresent()) {
                return new EspacoDTO(repository.findById(id).get());
            }
            return null;
        }catch( Exception e ){
            Log.enviaErro404(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    private List<EspacoDTO> convertList(Iterable<EspacoEntity> entidades ){
        ArrayList<EspacoDTO> espacosConvertidos = new ArrayList<EspacoDTO>();
        for( EspacoEntity entidade:entidades){
            espacosConvertidos.add( new EspacoDTO( entidade ) );
        }
        return espacosConvertidos;
    }

}