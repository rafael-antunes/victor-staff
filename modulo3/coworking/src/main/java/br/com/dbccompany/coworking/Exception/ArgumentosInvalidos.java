package br.com.dbccompany.coworking.Exception;

public class ArgumentosInvalidos extends Exception {
    private String message;

    public ArgumentosInvalidos(String message) {
        super(message);
    }

    public ArgumentosInvalidos(){}

    @Override
    public String getMessage() {
        return this.message;
    }
}
