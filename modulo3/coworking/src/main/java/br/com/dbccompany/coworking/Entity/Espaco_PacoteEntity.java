package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Espaco_PacoteEntity {

    @Id
    @SequenceGenerator( name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ")
    @GeneratedValue( generator= "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne( fetch = FetchType.LAZY)
    @JoinColumn( name = "pacote_id" )
    private PacoteEntity pacote;

    @ManyToOne
    @JoinColumn( name = "espaco_id" )
    private EspacoEntity espaco;

    @Column( nullable = false )
    private Tipo_ContratacaoEnum tipoContratacao;

    @Column( nullable = false )
    private Integer quantidade;

    @Column( nullable = false )
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public Tipo_ContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_ContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }
}
