package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.PagamentoDTO;
import br.com.dbccompany.coworking.DTO.PagamentoDTOResponse;
import br.com.dbccompany.coworking.DTO.PagamentoDTOResponseContratacao;
import br.com.dbccompany.coworking.DTO.PagamentoDTOResponsePacote;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Exception.ArgumentosInvalidos;
import br.com.dbccompany.coworking.Exception.ErroAoRealizarOperacao;
import br.com.dbccompany.coworking.Exception.ErroAoSalvar;
import br.com.dbccompany.coworking.Exception.RetornoDeBuscaInvalido;
import br.com.dbccompany.coworking.Repository.Cliente_PacoteRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.PagamentoRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository repository;

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Autowired
    private Cliente_PacoteRepository clientePacoteRepository;

    @Autowired
    private SaldoClienteService saldoClienteService;

    public List<PagamentoDTOResponse> trazerTodos() throws RetornoDeBuscaInvalido {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    @Transactional( rollbackFor = Exception.class )
    public PagamentoDTOResponse realizaPagamento ( PagamentoDTO pagamentoRecebido ) throws Exception {
        if(pagamentoRecebido.getContratacaoId() == null){
            if( pagamentoRecebido.getClientePacoteId() == null ){
                Exception e = new ArgumentosInvalidos();
                Log.enviaErro400( e , "Deve ser infomado Contratacao ou Cliente_Pacote" );
                throw e;
            }else{
                try{
                    return this.pagamentoPacote( pagamentoRecebido );
                }catch (Exception e){
                    Log.enviaErro400(e);
                    throw new ErroAoSalvar();
                }
            }
        }else if(  pagamentoRecebido.getClientePacoteId() == null  ){
            try{
                return this.pagamentoContratacao( pagamentoRecebido );
            }catch (Exception e){
                Log.enviaErro400(e);
                throw new ErroAoSalvar();
            }
        }else{
            Exception e = new ArgumentosInvalidos();
            Log.enviaErro400( e , "Deve ser infomado apenas Contratacao ou Cliente_Pacote" );
            throw e;
        }
    }

    private PagamentoDTOResponseContratacao pagamentoContratacao( PagamentoDTO pagamentoRecebido ){
        PagamentoEntity pagamento = pagamentoRecebido.convert();
        pagamento.setContratacao( contratacaoRepository.findById( pagamentoRecebido.getContratacaoId() ).get() );
        pagamento = repository.save( pagamento );
        saldoClienteService.pagarContratacao( pagamento.getContratacao() );
        PagamentoDTOResponseContratacao response = new PagamentoDTOResponseContratacao( pagamento );
        return response;
    }

    private PagamentoDTOResponsePacote pagamentoPacote ( PagamentoDTO pagamentoRecebido ){
        PagamentoEntity pagamento = pagamentoRecebido.convert();
        pagamento.setClientePacote( clientePacoteRepository.findById( pagamentoRecebido.getClientePacoteId() ).get() );
        pagamento = repository.save( pagamento );
        saldoClienteService.pagarClientePacote( pagamento.getClientePacote() );
        PagamentoDTOResponsePacote response = new PagamentoDTOResponsePacote( pagamento );
        return response;
    }

    @Transactional( rollbackFor = Exception.class)
    public PagamentoDTOResponse save( PagamentoDTO tipoContato ) throws ErroAoSalvar {
        try {
            return this.saveAndEdit( tipoContato.convert() );
        } catch (Exception e) {
            Log.enviaErro400(e);
            throw new ErroAoSalvar();
        }
    }

    private PagamentoDTOResponse saveAndEdit( PagamentoEntity tipoContato ){
        PagamentoEntity entity = repository.save( tipoContato );
        return new PagamentoDTOResponse( entity );
    }

    public PagamentoDTOResponse findById( Integer id) throws RetornoDeBuscaInvalido {
        try {
            Optional<PagamentoEntity> tipoContato = repository.findById(id);
            return new PagamentoDTOResponse(repository.findById(id).get());
        }catch( Exception e ){
            Log.enviaErro404(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    private List<PagamentoDTOResponse> convertList(Iterable<PagamentoEntity> entidades){
        ArrayList<PagamentoDTOResponse> elementosConvertidos = new ArrayList<PagamentoDTOResponse>();
        for( PagamentoEntity entidade:entidades) {
            if (entidade.getContratacao() == null) {
                elementosConvertidos.add(new PagamentoDTOResponsePacote(entidade));
            } else {
                elementosConvertidos.add(new PagamentoDTOResponseContratacao(entidade));
            }
        }
        return elementosConvertidos;
    }

}
