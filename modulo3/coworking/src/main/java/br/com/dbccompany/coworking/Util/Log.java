package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

public class Log {
    private static Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);
    private static RestTemplate restTemplate = new RestTemplate();
    private static String url = "http://localhost:8081/logger/salvar";

    public static void enviaErro400( Exception e ){
        String mensagemLogger = "Erro ao realizar requizição";
        System.err.println( e.getMessage() );
        logger.error(mensagemLogger);
        ErroDTO erro = new ErroDTO( e, "ERROR", mensagemLogger, "400" );
        restTemplate.postForObject(url, erro, Object.class);
    }

    public static void enviaErro400( Exception e, String mensagemLogger ){
        System.err.println( e.getMessage() );
        logger.error(mensagemLogger);
        ErroDTO erro = new ErroDTO( e, "ERROR", mensagemLogger, "400" );
        restTemplate.postForObject(url, erro, Object.class);
    }

    public static void enviaErro404( Exception e ){
        String mensagemLogger = "Elemento não encontrado";
        System.err.println( e.getMessage() );
        logger.error(mensagemLogger);
        ErroDTO erro = new ErroDTO( e, "ERROR", mensagemLogger, "404" );
        restTemplate.postForObject(url, erro, Object.class);
    }

    public static void enviaErro404( Exception e, String mensagemLogger ){
        System.err.println( e.getMessage() );
        logger.error(mensagemLogger);
        ErroDTO erro = new ErroDTO( e, "ERROR", mensagemLogger, "404" );
        restTemplate.postForObject(url, erro, Object.class);
    }




}
