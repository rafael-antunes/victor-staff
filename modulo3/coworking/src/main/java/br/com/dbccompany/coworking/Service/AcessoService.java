package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.DTO.AcessoDTOResponse;
import br.com.dbccompany.coworking.DTO.AcessoDTOSaldo;
import br.com.dbccompany.coworking.Entity.AcessoEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Entity.Tipo_ContratacaoEnum;
import br.com.dbccompany.coworking.Exception.ErroAoRealizarOperacao;
import br.com.dbccompany.coworking.Exception.ErroAoSalvar;
import br.com.dbccompany.coworking.Exception.RetornoDeBuscaInvalido;
import br.com.dbccompany.coworking.Repository.AcessoRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRespository;
import br.com.dbccompany.coworking.Util.Conversor;
import br.com.dbccompany.coworking.Util.Log;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository repository;

    @Autowired
    private SaldoClienteRespository saldoClienteRepository;

    public AcessoDTOSaldo registraAcesso(AcessoDTO acessoRecebido) throws ErroAoRealizarOperacao {
        try {
            AcessoEntity acesso = acessoRecebido.convert();
            SaldoClienteId saldoClienteId = new SaldoClienteId();
            saldoClienteId.setId_cliente(acessoRecebido.getClienteId());
            saldoClienteId.setId_espaco(acessoRecebido.getEspacoId());
            acesso.setSaldoCliente(saldoClienteRepository.findById(saldoClienteId).get());
            return acesso.getEntrada() ? entrar(acesso) : sair(acesso);
        }catch(Exception e){
            Log.enviaErro400( e );
            throw new ErroAoRealizarOperacao();
        }
    }

    private AcessoDTOSaldo entrar ( AcessoEntity acesso ){
        AcessoDTOSaldo response = new AcessoDTOSaldo();
        Integer saldo = acesso.getSaldoCliente().getQuantidade();
        if( saldo > 0 ){
            if( acesso.getData().isBefore(LocalDateTime.now()) ){
                repository.save( acesso );
                response.setSaldo( "Saldo: "+saldo.toString()+acesso.getSaldoCliente().getTipoContratacao().toString() );
            }else{
                response.setSaldo( "Saldo Vencido" );
            }
        }else{
            response.setSaldo( "Saldo Insuficiente" );
        }
        return response;
    }

    private AcessoDTOSaldo sair ( AcessoEntity acesso ){
        List<AcessoEntity> acessos = repository.findAllBySaldoClienteAndIsEntradaOrderByData(acesso.getSaldoCliente(), true);
        AcessoEntity ultimoAcesso = acessos.get(acessos.size() - 1);
        Duration tempoGasto = Duration.between(ultimoAcesso.getData(), acesso.getData());
        Tipo_ContratacaoEnum tipoContratacao = acesso.getSaldoCliente().getTipoContratacao();
        Integer saldoAnterior = acesso.getSaldoCliente().getQuantidade();
        Integer saldoFinal = saldoAnterior - Conversor.convertTempoGasto( tipoContratacao,  tempoGasto);
        acesso.getSaldoCliente().setQuantidade( saldoFinal );
        repository.save(acesso);

        AcessoDTOSaldo response = new AcessoDTOSaldo();
        response.setSaldo( "Saldo: "+saldoFinal+acesso.getSaldoCliente().getTipoContratacao().toString() );
        return response;
    }

    public List<AcessoDTOResponse> trazerTodos() throws RetornoDeBuscaInvalido {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public AcessoDTOResponse save( AcessoDTO acesso ) throws ErroAoSalvar {
        try {
            return this.saveAndEdit(acesso.convert());
        } catch (Exception e) {
            Log.enviaErro400(e);
            throw new ErroAoSalvar();
        }
    }

    private AcessoDTOResponse saveAndEdit( AcessoEntity acesso ){
        AcessoEntity acessoNew = repository.save( acesso );
        return new AcessoDTOResponse( acessoNew );
    }

    public AcessoDTOResponse findById( Integer id) throws RetornoDeBuscaInvalido {
        try {
            Optional<AcessoEntity> contato = repository.findById(id);
            return new AcessoDTOResponse(repository.findById(id).get());
        }catch( Exception e ){
            Log.enviaErro404(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    private List<AcessoDTOResponse> convertList( Iterable<AcessoEntity> entidades ){
        ArrayList<AcessoDTOResponse> convertidos = new ArrayList<AcessoDTOResponse>();
        for( AcessoEntity entidade:entidades){
            convertidos.add( new AcessoDTOResponse( entidade ) );
        }
        return convertidos;
    }

}
