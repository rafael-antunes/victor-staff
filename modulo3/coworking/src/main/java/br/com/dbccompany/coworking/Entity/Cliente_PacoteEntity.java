package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class Cliente_PacoteEntity {

    @Id
    @SequenceGenerator( name = "CLIENTE_PACOTE_SEQ", sequenceName = "CLIENTE_PACOTE_SEQ")
    @GeneratedValue( generator= "CLIENTE_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne( fetch = FetchType.LAZY)
    @JoinColumn( name = "pacote_id" )
    private PacoteEntity pacote;

    @ManyToOne
    @JoinColumn( name = "cliente_id" )
    private ClienteEntity cliente;

    @Column( nullable = false )
    private Integer quantidade;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
