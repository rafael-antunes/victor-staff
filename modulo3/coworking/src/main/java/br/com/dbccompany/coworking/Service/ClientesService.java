package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Exception.ErroAoRealizarOperacao;
import br.com.dbccompany.coworking.Exception.ErroAoSalvar;
import br.com.dbccompany.coworking.Exception.RetornoDeBuscaInvalido;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import br.com.dbccompany.coworking.Repository.Tipo_ContatoRepository;
import br.com.dbccompany.coworking.Util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository repository;

    @Autowired
    private ContatoRepository contatoRepository;

    @Autowired
    private Tipo_ContatoRepository tipoContatoRepository;

    public List<ClienteDTO> trazerTodosOsClientes() throws RetornoDeBuscaInvalido {
        try {
            return this.convertList(repository.findAll());
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public ClienteDTO save(ClienteDTO cliente ) throws RetornoDeBuscaInvalido, ErroAoSalvar {
        try {
            ArrayList<ContatoDTO> contatos = cliente.getContato();
            if (!contatos.get(0).getTipoContato().getNome().equals("email")) {
                return null;
            } else if (!contatos.get(1).getTipoContato().getNome().equals("telefone")) {
                return null;
            }

            ClienteEntity retorno = this.saveAndEdit(cliente.convert());

            for (ContatoDTO contato : contatos) {
                ContatoEntity contatoSalvar = contato.convert();
                contatoSalvar.setTipoContato(tipoContatoRepository.findByNome(contato.getTipoContato().getNome()));
                contatoSalvar.setCliente(retorno);
                contatoRepository.save(contatoSalvar);
            }
            return new ClienteDTO(retorno);
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new ErroAoSalvar();
        }
    }

    @Transactional( rollbackFor = Exception.class)
    public ClienteDTO edit(ClienteDTO cliente, Integer id ) throws ErroAoRealizarOperacao {
        try{
            ClienteEntity clienteEntity = cliente.convert();
            clienteEntity.setId(id);
            return new ClienteDTO( this.saveAndEdit( clienteEntity ) );
        }catch(Exception e){
            Log.enviaErro400(e);
            throw new ErroAoRealizarOperacao();
        }
    }

    private ClienteEntity saveAndEdit(ClienteEntity cliente ){
        return repository.save( cliente );
    }

    public ClienteDTO findById(Integer id) throws RetornoDeBuscaInvalido {
        try{
            Optional<ClienteEntity> cliente = repository.findById(id);
            return new ClienteDTO( repository.findById(id).get() );
        }catch( Exception e ){
            Log.enviaErro404(e);
            throw new RetornoDeBuscaInvalido();
        }
    }

    private List<ClienteDTO> convertList(Iterable<ClienteEntity> entidades ){
        ArrayList<ClienteDTO> clientesConvertidos = new ArrayList<ClienteDTO>();
        for( ClienteEntity entidade:entidades){
            clientesConvertidos.add( new ClienteDTO( entidade ) );
        }
        return clientesConvertidos;
    }

}
