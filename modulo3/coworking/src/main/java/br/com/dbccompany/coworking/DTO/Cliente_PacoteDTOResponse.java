package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Cliente_PacoteEntity;

public class Cliente_PacoteDTOResponse {
    private Integer id;
    private ClienteDTO cliente;
    private Integer quantidade;

    public Cliente_PacoteDTOResponse(){}

    public Cliente_PacoteDTOResponse(Cliente_PacoteEntity entity){
        this.id = entity.getId();
        this.cliente = new ClienteDTO( entity.getCliente() );
        this.quantidade = entity.getQuantidade();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
