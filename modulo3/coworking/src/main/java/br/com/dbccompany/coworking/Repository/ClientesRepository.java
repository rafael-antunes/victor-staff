package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientesRepository extends CrudRepository<ClienteEntity, Integer> {

    @Override
    Optional<ClienteEntity> findById(Integer integer);
    ClienteEntity findByNome( String nome );
}
