package br.com.dbccompany.coworking.Exception;

public class ErroAoSalvar extends Exception {
    private String message;

    public ErroAoSalvar(String message) {
        super(message);
    }

    public ErroAoSalvar() {
        super();
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}