package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class PagamentoEntity {

    @Id
    @SequenceGenerator( name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue( generator= "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    private Cliente_PacoteEntity clientePacote;

    @ManyToOne
    private ContratacaoEntity contratacao;

    private Tipo_PagamentoEnum tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cliente_PacoteEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(Cliente_PacoteEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public Tipo_PagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(Tipo_PagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
