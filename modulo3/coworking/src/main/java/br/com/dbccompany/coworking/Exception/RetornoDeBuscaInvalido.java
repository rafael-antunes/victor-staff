package br.com.dbccompany.coworking.Exception;

public class RetornoDeBuscaInvalido extends Exception {
    private String message;

    public RetornoDeBuscaInvalido(String message) {
        super(message);
    }

    public RetornoDeBuscaInvalido() {
        super();
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
