package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.Tipo_ContratacaoEnum;

public class ContratacaoDTO {
    private EspacoDTO espaco;
    private ClienteDTO cliente;
    private Tipo_ContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Double desconto;
    private Integer prazo;

    public ContratacaoDTO (){}

    public ContratacaoEntity convert(){
        ContratacaoEntity entity = new ContratacaoEntity();
        entity.setTipoContratacao( this.tipoContratacao );
        entity.setQuantidade( this.quantidade );
        entity.setDesconto( this.desconto );
        entity.setPrazo( this.prazo );
        return entity;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public Tipo_ContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_ContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
