package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Espaco_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;

import java.util.ArrayList;
import java.util.List;

public class PacoteDTO {
    private Integer id;
    private String valor;

    public PacoteDTO(){}

    public PacoteDTO(PacoteEntity entity) {
        this.id = entity.getId();
        this.valor = "R$"+entity.getValor();
    }

    public PacoteEntity convert (){
        PacoteEntity entity = new PacoteEntity();
        entity.setId( this.id );
        entity.setValor( Double.parseDouble( this.valor.substring(2) ) );
        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    private List<Espaco_PacoteEntity> convertDTOToEntity(List<Espaco_PacoteDTO> arrayDTO){
        ArrayList<Espaco_PacoteEntity> arrayEntity = new ArrayList<Espaco_PacoteEntity>();
        if(arrayDTO == null ){
            return null;
        }
        for( Espaco_PacoteDTO dto:arrayDTO){
            arrayEntity.add( dto.convert() );
        }
        return arrayEntity;
    }

    private List<Espaco_PacoteDTO> convertEntityToDTO ( List<Espaco_PacoteEntity> arrayEntity){
        ArrayList<Espaco_PacoteDTO> arrayDTO = new ArrayList<Espaco_PacoteDTO>();
        if(arrayEntity == null ){
            return null;
        }
        for( Espaco_PacoteEntity entidade:arrayEntity){
            arrayDTO.add( new Espaco_PacoteDTO( entidade ) );
        }
        return arrayDTO;
    }

}