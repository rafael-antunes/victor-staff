let pokeApi = new PokeApi();

let renderizarPokemonBuscado = () => {
  let idAtual = dadosPokemon.querySelector(".id").innerHTML;
  idAtual = (idAtual.split(" "))[1];
  let number = inputBuscaPorId.value;

  if( idAtual == number){
    return ;
  }
  if( number > 0 && number < 893 ) {
    pokeApi.buscarEspecífico(number)
      .then( pokemon => {
          let poke = new Pokemon( pokemon );
          renderizar( poke );
    });
  }else{
    if( number == '' ){
      clearPokemon();
      return;
    }
    let mensagemErro = document.getElementById("mensagem-de-erro");
    mensagemErro.className = '';
    setTimeout( () => {
      mensagemErro.className = 'mensagem-escondida';
    }, 3000);
  }
}

let feelingLuckyAction = () => {
  let pokemonNumber;
  let idAtual = dadosPokemon.querySelector(".id").innerHTML;
  idAtual = (idAtual.split(" "))[1];

  let gerarNovoNumero = true;

  while( gerarNovoNumero ) {
    pokemonNumber = Math.round(Math.random() * (893-1) +1);

    if( idAtual != pokemonNumber ){
      gerarNovoNumero = podeBuscarComSorte ( pokemonNumber ) ? false : true;
    }
  }

  pokeApi.buscarEspecífico(pokemonNumber)
  .then( pokemon => {
    let poke = new Pokemon( pokemon );
    renderizar( poke );
  });
}

let renderizar = ( pokemon ) => {
  let dadosPokemon = document.getElementById("dadosPokemon");

  inputBuscaPorId.value = pokemon.id;

  let nome = dadosPokemon.querySelector(".nome");
  nome.innerHTML = pokemon.nome;

  let imagem = dadosPokemon.querySelector(".thumb");
  imagem.src = pokemon.imagem;

  let altura = dadosPokemon.querySelector(".altura");
  altura.innerHTML = `Altura: ${ pokemon.altura }`;

  let id = dadosPokemon.querySelector(".id");
  id.innerHTML = `ID: ${ pokemon.id }`;

  let peso = dadosPokemon.querySelector(".peso");
  peso.innerHTML = `Peso: ${ pokemon.peso }`;

  let headerTipo = dadosPokemon.querySelector(".header-tipo");
  headerTipo.innerHTML = pokemon.tipos.length == 1 ? "Tipo" : "Tipos";

  let tipoUl =dadosPokemon.querySelector(".tipo");
  tipoUl.innerHTML = '';

  for (let tipo of pokemon.tipos) {
    let novoLi = document.createElement("li");
    novoLi.className = "result-section-li";
    let conteudoLi = document.createTextNode(tipo.type.name);
    novoLi.appendChild( conteudoLi );
    tipoUl.appendChild( novoLi );
  }

  dadosPokemon.querySelector(".header-estatisticas").innerHTML = "Estatisticas";

  let estatisticaUl =dadosPokemon.querySelector(".estatistica");
  estatisticaUl.innerHTML = '';

  for ( let estatistica of pokemon.estatisticas ) {
    let novoLi = document.createElement("li");
    novoLi.className = "result-section-li";
    let conteudoLi = document.createTextNode(`${ estatistica.stat.name }: ${ estatistica.base_stat }`);
    novoLi.appendChild( conteudoLi );
    estatisticaUl.appendChild( novoLi );
  }

  dadosPokemon.className += " result-section";

}

let clearPokemon = () => {
  let dadosPokemon = document.getElementById("dadosPokemon");

  inputBuscaPorId.value = '';
  let nome = dadosPokemon.querySelector(".nome");
  nome.innerHTML = '';
  let imagem = dadosPokemon.querySelector(".thumb");
  imagem.src = '';
  let altura = dadosPokemon.querySelector(".altura");
  altura.innerHTML = '';
  let id = dadosPokemon.querySelector(".id");
  id.innerHTML = '';
  let peso = dadosPokemon.querySelector(".peso");
  peso.innerHTML = '';
  let headerTipo = dadosPokemon.querySelector(".header-tipo");
  headerTipo.innerHTML = '';
  let tipoUl =dadosPokemon.querySelector(".tipo");
  tipoUl.innerHTML = '';

  dadosPokemon.querySelector(".header-estatisticas").innerHTML = '';

  let estatisticaUl =dadosPokemon.querySelector(".estatistica");
  estatisticaUl.innerHTML = '';

  let mensagemErro = document.getElementById("mensagem-de-erro");
  mensagemErro.className = 'mensagem-escondida';
  dadosPokemon.className = "col col-12 center";
}

let podeBuscarComSorte = ( number ) => {
  let cache = localStorage.getItem("luckyIds");
  if( cache == null ){
    localStorage.setItem("luckyIds", number);
    return true;
  }
  arrCache = cache.split(",");
  if( arrCache.includes( number ) ) {
    return false;
  }else{
    cache+=","+number;
    localStorage.setItem("luckyIds", cache);
    return true;
  }

}

let inputBuscaPorId = document.getElementById("buscar-por-id");
inputBuscaPorId.addEventListener("blur", renderizarPokemonBuscado );

let feelingLuckyButton = document.getElementById("feeling-lucky-button");
feelingLuckyButton.addEventListener("click", feelingLuckyAction);

let clearButton = document.getElementById("clear-button");
clearButton.addEventListener("click", clearPokemon);