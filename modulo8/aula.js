console.log("Chegou aqui");

/* VAR */
console.log(nome);
var nome = "Nome Exemplo";
var nome = "Mudei o valor";
console.log(nome);


/* LET */ 
let nome1 = "Exemplo de let";

  {
    let nome1 = "blabla";
    nome1 = "Dentro do escopo";
    console.log(nome1);
  }
console.log(nome1);

/* CONST */
//const nome2 = "Mais um exemplo";
const pessoa = {
  nome: "Nome"
};

console.log(pessoa.nome);

Object.freeze(pessoa);

pessoa.nome = "OUTRO NOME";
//pessoa.idade = "20";

console.log(pessoa.nome);
//console.log(pessoa.idade);

/* ESPETACULAR DO MARCOS */

let soma = 1 + 2;
let soma1 = "1" + 2 + 3;
let soma2 = 1 + "2" + 3;
let soma3 = 1 + 2 + "3";

console.log(soma);
console.log(soma1);
console.log(soma2);
console.log(soma3);


/* FUNÇÕES */

let nomeFuncao = "Marcos";
let idadeFuncao = 31;
let semestre = 5;
let notas = [ 10.0, 3, 5, 9 ]; 

function funcaoCriarAluno(nomeFuncao, idadeFuncao, semestre, notas = []) {
  const aluno = {
    nome: nomeFuncao,
    idade: idadeFuncao,
    semestre: semestre,
    nota: notas
  };

  function aprovadoOrReprovado( notas ) {
    if( notas.length == 0 ){
      return "Sem Notas";
    }

    let somatorio = 0;
    for (let i = 0; i < notas.length; i++) {
      somatorio += notas[i];
    }

    return(somatorio / notas.length ) > 7.0 ? "Aprovado" : "Reprovado";

  }

  aluno.status = aprovadoOrReprovado( notas );
  return aluno;
}

let alunoExterno = funcaoCriarAluno(nomeFuncao, idadeFuncao, semestre, notas);
console.log( alunoExterno );
alunoExterno = funcaoCriarAluno(nomeFuncao, idadeFuncao, semestre);
console.log( alunoExterno );

/* Template String */
let texto = "Testo";
let outroTexto = "Texto 2";

console.log( `${texto} coisas no meio ${ outrovalor } - ${ pessoa.nome } ` );

console.log( `${texto} 
coisas no meio com quebras de linha  
${ outrovalor } - ${ pessoa.nome } ` );

/*   Destruction   */

let objeto = {
  nome: "Marcos",
  idade: 31, 
  altura: 1.85
}

const { nome, altura } = objeto;

const arrayTeste = ['josias', 'adalberto', 'gonofredo', 'jeremias'];

let [, pos2,, posi4] = arrayTeste;

let a = 1;
let b = 3;

[a,b] = [b,a]; 

/**  SPREAD  */

let arraySpread = [1,77,83,42];
let objetoSpread = {...arraySpread};
console.log ( ...arraySpread );
console.log( { ...arraySpread } );

