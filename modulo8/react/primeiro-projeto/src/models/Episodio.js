import EpisodioAPI from "./EpisodioAPI";

export default class Episodio{
  constructor( { id, nome, duracao, temporada, ordemEpisodio, thumbUrl, nota } ) {
    this.id = id;
    this.nome = nome;
    this.duracao = duracao;
    this.temporada = temporada;
    this.ordem = ordemEpisodio;
    this.imagem = thumbUrl;
    this.qtdVezesAssistido = 0;
    this.episodioApi = new EpisodioAPI();
    this.nota = 0;
  }

  get duracaoEmMin() {
    return `${ this.duracao } min`;
  }

  get temporadaEpisodio() {
    return  `${ this.temporada.toString().padStart( 2 , 0 ) }/${ this.ordem.toString().padStart( 2 , 0 ) }`;
  }

  adicionarUmaAssistida() {
    this.foiAssistido = true;
    this.qtdVezesAssistido += 1 ;
  }

  avaliar( nota ) {
    return this.episodioApi.registrarNota( { nota: nota, episodioId: this.id } ).then( this.atualizarNota() );
  }

  validarNota( nota ) {
    return ( nota>=1 && nota<=5 );
  }
//precisa melhorar performance
  atualizarNota(){
    return this.episodioApi.buscarNota( this.id ).then( notasPorId => {
      console.log( notasPorId );
      let totalNotas = 0;
      notasPorId.forEach( episode => {
        totalNotas += parseInt( episode.nota );  
      });
      this.nota = totalNotas/notasPorId.length;
    } );     
  }

}
