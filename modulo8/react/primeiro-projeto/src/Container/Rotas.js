import React, { Component } from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';

import App from './App';
import ListaAvaliacoes from './Avaliacoes/ListaAvaliacoes';
import TelaDetalheEpisodio from './TelaDetalheEpisodio/TelaDetalheEpisodio';
import TodosEpisodios from './TodosEpisodios';
import Formulario from './Formulario';

import { RotasPrivadas } from '../Components/RotasPrivadas';

export default class Rotas extends Component {

  render() {
    return (
      <Router>
        <Route path="/" exact component={ App } />
        <Route path="/avaliacoes" exact component={ ListaAvaliacoes } />
        <Route path="/formulario" exact component={ Formulario } />
        <RotasPrivadas path="/episodios" exact component={ TodosEpisodios }/>
        <Route path="/episodio/:id" exact component={ TelaDetalheEpisodio }/>
      </Router>
    );
  }
}
