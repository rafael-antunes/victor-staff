import React, { Component } from 'react';

import MensagemFlash from '../Components/MensagemFlash';
import EpisodioUi from '../Components/EpisodioUi';
import MeuInputNumero from '../Components/MeuInputNumero/index';
import BotaoUi from '../Components/BotaoUi/BotaoUi';

import Mensagem from '../Contantes/mensagem';

import EpisodioAPI from '../models/EpisodioAPI';
import ListaEpisodios from '../models/ListaEpisodios';

import './App.css';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.episodioAPI = new EpisodioAPI();
    this.sortear = this.sortear.bind(this);
    this.marcarComoAssistido = this.marcarComoAssistido.bind(this);
    this.registrarNota = this.registrarNota.bind(this);
    this.state = {
      deveExibirMensagem: false,
      deveExibirErro: false
    }
  }

  componentDidMount() {
    this.episodioAPI.buscar()
      .then( episodios => {
        this.listaEpisodios = new ListaEpisodios( episodios );
        const episodio = this.listaEpisodios.episodioAleatorio;
        episodio.atualizarNota().then( 
          this.setState( state => { 
            console.log( episodio.nota );
            return { ...state, episodio } } )
         )

      } )
  }

  sortear() {
    const episodio = this.listaEpisodios.episodioAleatorio;

    this.setState((state) => {
      return {
        ...state,
        episodio
      }
    });

  }

  marcarComoAssistido() {
    const { episodio } = this.state;
    episodio.adicionarUmaAssistida();
    this.setState((state) => {
      return {
        ...state,
        episodio
      }
    });
  }

  registrarNota( { erro, nota } ) {

    this.setState(( state ) => {
      return {
        ...state,
        deveExibirErro: erro
      }
    });
    if( erro ) {
      return;
    }
    const { episodio } = this.state;
    let mensagem, corMensagem;

    if (episodio.validarNota( nota )) {
      episodio.avaliar( nota ).then( () => {
        corMensagem = 'verde';
        mensagem = 'Nota registrada com sucesso!';
        this.exibirMensagem( { corMensagem, mensagem } );     
      } );

    } else {
      corMensagem = 'vermelho';
      mensagem = 'Informar uma nota válida (entre 1 e 5)';
      this.exibirMensagem( { corMensagem, mensagem } );
    }
    
  }

  exibirMensagem = ( { corMensagem, mensagem } ) => {
    this.setState(( state ) => {
      return {
        ...state,
        deveExibirMensagem: true,
        mensagem,
        corMensagem
      }
    });
  }

  atualizarMensagem = devoExibir => {
    this.setState((state) => {
      return {
        ...state,
        deveExibirMensagem: devoExibir
      }
    });
  }

  render() {
    const { episodio, deveExibirErro, deveExibirMensagem, mensagem, corMensagem } = this.state;
    const { listaEpisodios } = this;
 
    return ( 
      !episodio ? 
        ( <h3>Aguarde...</h3> ) :
        ( <div className="App" >
          <BotaoUi className='verde' text='Lista de Avaliações' link={ { pathname: "/avaliacoes", state: { listaEpisodios } } } />
          <MensagemFlash atualizar={this.atualizarMensagem} exibir={deveExibirMensagem} mensagem={mensagem} cor={corMensagem} />
          <header className="App-header">
            <EpisodioUi episodio={episodio} />
            <div className="botoes">
              <BotaoUi className="verde" onClick={this.sortear} text="Próximo"/>
              <BotaoUi className="azul" onClick={this.marcarComoAssistido} text="Já assisti"/>
            </div>
            {  }
            { console.log( `Dentro do render : ${ episodio.nota }` )}
            <span>Nota: { episodio.nota.toFixed(2) }</span>
            <MeuInputNumero 
              placeholder = 'Nota de 1 a 5' 
              mensagem = 'Avalie o Episodio: ' 
              exibir = {episodio.foiAssistido || false}
              obrigatorio
              exibirErro={ deveExibirErro }
              atualizarValor = { this.registrarNota } 
            />
          </header>
        </div> )
    );
  }
}
