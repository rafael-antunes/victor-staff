import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstrategiaAtkIntercaladoTest {
    
    @Test
    public void ordenaExercitoIntercalado(){
        EstrategiaAtkIntercalado estrategia = new EstrategiaAtkIntercalado();
        
        ArrayList<Elfo> elfos = new ArrayList<>();
        elfos.add (new ElfoNoturno( "Alfredo" ) );
        elfos.add( new ElfoVerde( "Josue" ) );
        elfos.add( new ElfoVerde( "Roberta" ) );
        elfos.add( new ElfoVerde( "Josiane" ) );
        elfos.add( new ElfoVerde( "Josiane" ) );
        elfos.add( new ElfoVerde( "Josiane" ) );
        elfos.add (new ElfoNoturno( "Andrei" ) );
        
        ArrayList<Elfo> elfosOrdenados = estrategia.getOrdemDeAtaque( elfos );
        
        assertEquals( 4, elfosOrdenados.size() );
        assertEquals( elfosOrdenados.get(0).getClass() , ElfoVerde.class ); 
        assertEquals( elfosOrdenados.get(1).getClass() , ElfoNoturno.class ); 
        assertEquals( elfosOrdenados.get(2).getClass() , ElfoVerde.class ); 
        assertEquals( elfosOrdenados.get(3).getClass() , ElfoNoturno.class ); 
    }    
    
}
