<<<<<<< HEAD


=======
>>>>>>> otherrep/hipotese-alternativa
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

<<<<<<< HEAD
public class ElfoNoturnoTest{
   
    @Test 
    public void elfoNoturnoAtiraFlechaGanha3XPPerde15Vida(){
        ElfoNoturno elfoNoturno = new ElfoNoturno( "Legolas" );
        Dwarf anao = new Dwarf( "Anaozin" );
        elfoNoturno.atirarFlecha(anao);
        assertEquals(3, elfoNoturno.getExperiencia());       
        assertEquals(85, elfoNoturno.getVida(), 1e-8);  
    } 
}
=======
public class ElfoNoturnoTest {

    @Test
    public void elfoNoturnoGanha3XpPorFlecha(){
        ElfoNoturno noturno = new ElfoNoturno("Noturno");
        noturno.atirarFlecha(new Anao("Balin"));
        assertEquals( 3, noturno.getExperiencia() );
    }
    
    @Test
    public void elfoNoturnoPerde15HPPorFlecha(){
        ElfoNoturno noturno = new ElfoNoturno("Noturno");
        noturno.atirarFlecha(new Anao("Balin"));
        assertEquals( 85.0, noturno.getVida(), 1e-9 );
        assertEquals( Status.SOFREU_DANO, noturno.getStatus() );
    }
    
    @Test
    public void elfoNoturnoAtira7FlechasEMorre(){
        ElfoNoturno noturno = new ElfoNoturno("Noturno");
        noturno.getInventario().obter(1).setQuantidade(1000);
        
        noturno.atirarFlecha(new Anao("Balin"));
        noturno.atirarFlecha(new Anao("Balin"));
        noturno.atirarFlecha(new Anao("Balin"));
        noturno.atirarFlecha(new Anao("Balin"));
        noturno.atirarFlecha(new Anao("Balin"));
        noturno.atirarFlecha(new Anao("Balin"));
        noturno.atirarFlecha(new Anao("Balin"));
        
        assertEquals( .0, noturno.getVida(), 1e-9 );
        assertEquals( Status.MORTO, noturno.getStatus() );
    }
}
>>>>>>> otherrep/hipotese-alternativa
