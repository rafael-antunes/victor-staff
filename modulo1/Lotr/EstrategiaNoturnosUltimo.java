import java.util.*;

public class EstrategiaNoturnosUltimo implements EstrategiaAtk {
    public ArrayList<Elfo> getOrdemDeAtaque( ArrayList<Elfo> atacantes ) {
        ArrayList<Elfo> atacantesOrdenados = new ArrayList<>();
        
        for (Elfo atacante : atacantes ){
            if ( estaVivo( atacante ) ) {
                if( atacante instanceof ElfoNoturno ){
                    atacantesOrdenados.add(atacante);
                }else if( atacante instanceof ElfoVerde ){
                    atacantesOrdenados.add(0, atacante);
                }    
            }    
        }    
        return atacantesOrdenados;
    }
    
    private boolean estaVivo(Elfo elfo) {
        return elfo.getStatus() != Status.MORTO;
    }
}