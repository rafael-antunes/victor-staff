import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstrategiaTercoNoturnoTest{
    
    @Test
    public void RecebeElfosEOrdena(){
        EstrategiaTercoNoturno estrategia = new EstrategiaTercoNoturno();
        
        ArrayList<Elfo> elfos = new ArrayList<>();
        ArrayList<Elfo> elfosOrdenados = new ArrayList<>();
        
        
        elfos.add( new ElfoNoturno( "noturno1" ) );
        elfos.add( new ElfoNoturno( "noturno2" ) );        
        elfos.add( new ElfoNoturno( "noturno3" ) );        
        elfos.add( new ElfoNoturno( "noturno4" ) );        
        elfos.add( new ElfoNoturno( "noturno5" ) );        
        elfos.add( new ElfoNoturno( "noturno6" ) );        
        elfos.add( new ElfoVerde( "verde1" ) );
        elfos.add( new ElfoVerde( "verde2" ) );
        elfos.add( new ElfoVerde( "verde3" ) );
        elfos.add( new ElfoVerde( "verde4" ) );
        elfos.add( new ElfoVerde( "verde5" ) );
        elfos.add( new ElfoVerde( "verde6" ) );
        elfos.add( new ElfoVerde( "verde7" ) );
        elfos.add( new ElfoVerde( "verde8" ) );
        elfos.add( new ElfoVerde( "verde9" ) );
        elfos.add( new ElfoVerde( "verde10" ) );
        
        elfosOrdenados = estrategia.getOrdemDeAtaque( elfos );
        
        assertEquals( 13, elfosOrdenados.size() );
        
    }    
    
    
}
