import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstrategiaNoturnosUltimoTest
{
    @Test
    public void recebeContingenteRetornaOrdenado(){
        EstrategiaNoturnosUltimo estrategia = new EstrategiaNoturnosUltimo();
        
        ArrayList<Elfo> elfos = new ArrayList<>();
        Elfo elfo0 = new ElfoNoturno( "Alfredo" );
        elfos.add (elfo0);
        Elfo elfo1 = new ElfoVerde( "Josue" );
        elfos.add(elfo1);

        ArrayList<Elfo> elfosOrdenados = estrategia.getOrdemDeAtaque( elfos );
        
        assertEquals(elfosOrdenados.get(0), elfo1);
        assertEquals(elfosOrdenados.get(1), elfo0);


    }    
}
