import java.util.*;

public class EstrategiaAtkIntercalado implements EstrategiaAtk {
        public ArrayList<Elfo> getOrdemDeAtaque( ArrayList<Elfo> atacantes ) {
            ArrayList<Elfo> elfosOrdenados = new ArrayList<>();
            
            ArrayList<Integer> indiceElfosNoturnos = new ArrayList<>();
            ArrayList<Integer> indiceElfosVerdes = new ArrayList<>();
            
            for( int i=0 ; i < atacantes.size() ; i++){
                if ( atacantes.get(i) instanceof ElfoVerde ){
                    indiceElfosVerdes.add(i);
                } else {
                    indiceElfosNoturnos.add(i);
                }    
            }
            
            int menorIndice = indiceElfosVerdes.size() > indiceElfosNoturnos.size() ? indiceElfosNoturnos.size() : indiceElfosVerdes.size() ; 
            
            for ( int i=0 ; i < menorIndice ; i++){
                elfosOrdenados.add(atacantes.get(indiceElfosVerdes.get(i)));
                elfosOrdenados.add(atacantes.get(indiceElfosNoturnos.get(i)));
            }   
            
            return elfosOrdenados;
        }
}
