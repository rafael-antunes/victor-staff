<<<<<<< HEAD
import java.util.*;

public class EstatisticasInventario{
    private Inventario inventario;
    
    public EstatisticasInventario(Inventario inventarioRecebido){
        this.inventario = inventarioRecebido;
    }
    
    private boolean EVazio(){
        if( this.inventario.getInventario().isEmpty() ){
            return true;
        }else{
            return false;
        }    
    }
    
    public double calcularMedia(){
        if( EVazio() ){
            return Double.NaN;
        }
        int i=0, somaQuantidades = 0;
        
        for ( Item item: this.inventario.getInventario()){
            somaQuantidades += item.getQuantidade();
        }
        return somaQuantidades / (this.inventario.getInventario().size());
    }
    
    public double calcularMediana(){
        if( EVazio() ){
            return Double.NaN;
        }
        
        this.inventario.ordenarItens();
        int qtdItens = inventario.getTamInventario();
        
        if( qtdItens % 2 == 0){
            return ( this.somaQuantidadesMediasTamanhoPar( qtdItens ) / 2 );
        }else{
            return this.getQuantidadeItem( qtdItens / 2  );
        }    
    }
    
    private int getQuantidadeItem (int posicao){
        return inventario.obter( posicao ).getQuantidade();
    }
    
    private int somaQuantidadesMediasTamanhoPar(int qtdItens){
        int indiceMedio = qtdItens/2;
        return this.getQuantidadeItem( indiceMedio - 1 ) + this.getQuantidadeItem( indiceMedio);
    }
       
    public int qtdItensAcimaDaMedia (){ 
        int numItensAcimaMedia = 0;
        double mediaItens = this.calcularMedia();
        for (Item item : this.inventario.getInventario()){
            if( item.getQuantidade() > mediaItens ){
                numItensAcimaMedia++;
            }    
        }
        return numItensAcimaMedia;
    }
}
=======
public class EstatisticasInventario {
    private Inventario inventario;
    
    public EstatisticasInventario( Inventario inventario ) {
        this.inventario = inventario;
    }
    
    private boolean EVazio(){
        return this.inventario.getItens().isEmpty();
    }
    
    public double calcularMedia() {
        if( this.EVazio() ) {
            return Double.NaN;
        }
        
        double somaQtds = 0;
        for ( Item item : this.inventario.getItens() ) {
            somaQtds += item.getQuantidade();
        }
        
        return somaQtds / inventario.getItens().size();
    }
    
    public double calcularMediana() {
        if( this.EVazio() ) {
            return Double.NaN;
        }
        
        //TODO: ordenar inventario;
        int  qtdItens = this.inventario.getItens().size();
        int meio = qtdItens / 2;
        int qtdMeio = this.inventario.obter( meio ).getQuantidade();
        boolean qtdImpar = qtdItens % 2 == 1;
        if( qtdImpar ) {
            return qtdMeio;
        }
        
        int qtdMeioMenosUm = this.inventario.obter( meio - 1 ).getQuantidade();
        return ( qtdMeio + qtdMeioMenosUm ) / 2.0;
    }
    
    public int qtdItensAcimaDaMedia() {
        double media = this.calcularMedia();
        int qtdAcima = 0;
        
        for ( Item item : this.inventario.getItens() ) {
            if( item.getQuantidade() > media ){
                qtdAcima++;
            }
        }
        
        return qtdAcima;
    }
}
>>>>>>> otherrep/hipotese-alternativa
