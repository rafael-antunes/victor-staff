import java.util.*;

<<<<<<< HEAD
public class Inventario{
    private ArrayList <Item> itens;
     
    public Inventario(){
        this.itens = new ArrayList<>();
    }
    
    public ArrayList<Item> getInventario(){
        return itens;
    }    
    
    public int getTamInventario(){
        return this.itens.size();
    }
    
    public String getDescricaoItem(int posicao){
        return this.obter( posicao ).getDescricao();
    }
    
    public int getQuantidadeItem(int posicao){
        return this.obter( posicao ).getQuantidade();
    }
    
    public void adicionar(Item itemAdicionar){
        this.itens.add( itemAdicionar );
    }   
    
    private boolean validaPosicao(int posicao){
        return posicao >=this.itens.size();
    }
    
    public Item obter( int posicao){
        return posicao >= this.itens.size() ? null : this.itens.get(posicao);
    }
        
    public Item buscar( String descricao ){
        for (Item item : this.itens ){
            if( item.getDescricao().equals(descricao) ){
                return item;
            }    
        }
        return null;
    }
    
    public void remover ( Item item ){
        for ( int i = 0 ; i < this. getTamInventario() ; i++ ){
            if( this.getDescricaoItem(i).equals(item.getDescricao()) ){
                this.remover(i);
                return;
            }    
        }
    }
    
    public void remover (int posicao){
        if(!this.validaPosicao( posicao )){
            this.itens.remove( posicao );
        }       
    }
    
    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        for(Item item : this.itens){
            String descricao = item.getDescricao();
            descricoes.append(descricao);
            descricoes.append(",");
            }
        if(!this.itens.isEmpty()){
            descricoes.deleteCharAt(descricoes.length()-1);
        }
        return descricoes.toString();
    }
       
    public Item getItemMaiorQuantidade(){
        int indice = 0, maiorQuantidadeParcial = 0;
        for(int i=0; i < this.itens.size(); i++){
            int qtdAtual = this.obter(i).getQuantidade();
            if(qtdAtual > maiorQuantidadeParcial) {
                maiorQuantidadeParcial = qtdAtual;
                indice = i;
            }
        }
        return this.itens.size() > 0 ? this.obter(indice) : null;
    }
        
    public ArrayList<Item> inverter (){
        ArrayList<Item> itensInvertidos = new ArrayList<>(this.itens.size());
        for (int i = this.itens.size() ; i>0; i--){
            itensInvertidos.add(itens.get(i-1));
        }
        return itensInvertidos;
    }
        
    public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
    private void trocaElementos( int indice1, int indice2){
        Item itemMaisAlto = itens.get(indice1);
        itens.set(indice1, itens.get(indice2));
        itens.set(indice2, itemMaisAlto); 
    }
    
    public void ordenarItens(TipoOrdenacao tipoOrdenacao){
        int numItens = this.itens.size(), indiceTrocar, quantidadeAtual;
        boolean isAsc = tipoOrdenacao == TipoOrdenacao.ASC ? true : false;
        if(isAsc){
            for (int i=0; i < numItens ; i++){
                indiceTrocar = i; 
                quantidadeAtual = this.getQuantidadeItem(i);
                for(int j=i+1; j < numItens ; j++){
                    if( quantidadeAtual > this.getQuantidadeItem(j) ){
                        indiceTrocar = j;
                        quantidadeAtual = this.getQuantidadeItem(j);
                    }    
                }
                this.trocaElementos( i , indiceTrocar);
            }
        }else{
            for (int i=0; i < numItens ; i++){
                indiceTrocar = i;    
                quantidadeAtual = this.getQuantidadeItem(i);
                for(int j=i+1; j < numItens ; j++){
                    if( quantidadeAtual < this.getQuantidadeItem(j) ){
                        indiceTrocar = j;
                        quantidadeAtual = this.getQuantidadeItem(j);
                    }    
                }
                this.trocaElementos( i , indiceTrocar);
=======
public class Inventario {
    private ArrayList<Item> itens;
    
    public Inventario() {
        this.itens = new ArrayList<>();
    }
    
    public ArrayList<Item> getItens() {
        return this.itens;
    }
    
    public void adicionar( Item item ) {
        this.itens.add( item );
    }
    
    private boolean validaSePosicao( int posicao ) {
        return posicao >= this.itens.size();
    }
    
    public Item obter( int posicao ) {
        return this.validaSePosicao( posicao ) ? null : this.itens.get(posicao);
    }
    
    public void remover( int posicao ) {
        if( !this.validaSePosicao( posicao ) ) {
            this.itens.remove(posicao);
        }
    }
    
    public void remover( Item item ) {
        this.itens.remove( item );
    }
    
    public String getDescricoesItens() {
        StringBuilder descricoes = new StringBuilder();
        int i = 0;
        for( Item item : this.itens ) {
            String descricao = item.getDescricao();
            descricoes.append(descricao);
            //descricoes.append(",");
            
            i++;
            boolean deveColocarVirgula = i < this.itens.size() - 1;
            if( deveColocarVirgula ) {
                descricoes.append(",");
            }
        }
        
        return descricoes.toString();
    }
    
    public Item getItemComMaiorQuantidade() {
        int indice = 0, maiorQuantidadeParcial = 0;
        for ( int i = 0; i < this.itens.size(); i++ ) {
            if( this.itens.get(i) != null ) {
                int qtdAtual = this.itens.get(i).getQuantidade();
                if( qtdAtual > maiorQuantidadeParcial ) {
                    maiorQuantidadeParcial = qtdAtual;
                    indice = i;
                }
            }
        }
        
        //Ternario (NÃO É TERNEIRO) -> comparacao ? verdeiro : falso
        return this.itens.size() > 0 ? this.obter( indice ) : null;
    }
    
    public Item buscar( String descricao ) {
        for( Item item : this.itens ) {
            if( item.getDescricao().equals(descricao) ) {
                return item;
            }
        }
        return null;
    }
    
    public ArrayList<Item> inverter() {
        ArrayList<Item> listaInvertida = new ArrayList<>();
        
        for( int i = this.itens.size() - 1; i >= 0; i--) {
            listaInvertida.add( this.itens.get(i) );
        }
        
        return listaInvertida;
    }
    
    public void ordenarItens() {
        this.ordenarItens( TipoOrdenacao.ASC );
    }
    
    public void ordenarItens( TipoOrdenacao ordenacao ) {
        for( int i = 0; i < this.itens.size(); i++ ) {
            for( int j = 0; j < this.itens.size() - 1; j++ ) {
                Item atual = this.itens.get(j);
                Item proximo = this.itens.get(j+1);
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ?
                ( atual.getQuantidade() > proximo.getQuantidade() ) :
                ( atual.getQuantidade() < proximo.getQuantidade() );
                if( deveTrocar ) {
                    Item itemTrocado = atual;
                    this.itens.set(j, proximo);
                    this.itens.set(j + 1, itemTrocado);
                }
>>>>>>> otherrep/hipotese-alternativa
            }
        }
    }
}